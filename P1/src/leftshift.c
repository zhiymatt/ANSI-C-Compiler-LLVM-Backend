#include <stdio.h>
void main()
{
	const int length = 20;
	int amount;
	int array[length];
	int i;
	int temp;
	int mid;
	i = 0;
	while(i < length)
	{
		array[i] = i;
		i = i + 1;
    }
    printf("origin array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\nplease input leftshift amount:\n");
    scanf("%d", &amount);
    amount = amount % length;
	mid = (amount - 1) / 2;
	i = 0 ;
	while(i <= mid)
	{
		int new;
		new = amount - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
	mid = (length + amount - 2) / 2;
	i = amount ;
	while(i <= mid)
	{
		int new;
		new = length + amount - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
	mid = (length - 1) / 2;
	i = 0;
	while(i <= mid)
	{
		int new;
		new = length - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
    printf("the final array is:\n");
    for (i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
	
