#include <stdio.h>
void main()
{
	int i;
	int j;
	int length;
	const int a[] = {7, 5, 2, 9, 8, 6, 0, 3, 1};
	int val;
	int ind;
	int med;
	length = 9;
	int array[length];
	i = 0;
	while (i < length)
	{
		array[i] = a[i];
		i = i + 1;
	}
	i = 0;
	while (i < length - 1)
	{
		val = array[i];
		ind = i;
		j = i + 1;
		while (j < length)
		{
			if (array[j] < val)
			{
				val = array[j];
				ind = j;
			}
			j = j + 1;
		}
		med = array[ind];
		array[ind] = array[i];
		array[i] = med;
		i = i + 1;
	}
    printf("the origin array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",a[i]);
    printf("\n");
    printf("the sorted array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
