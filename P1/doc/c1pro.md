#leftshift.c文件
把用while循环生成的数组<0,1,2,...19>循环左移n位，其中n由用户指定。<br>
		
		设计思路：
			leftshift循环左移包含三个步骤。
			1. 对前n项构成的数组进行reverese
			2. 对剩余的项构成的数组进行reverse
			3. 对整个数组进行reverse
		
		问题：
			这里主要的问题时c1的函数不能带参数，所以把相似的代码重复写3次。	
			而while循环可以替代for循环，所以没什么影响。
			因为c1里没有动态内存分配，所以用的是定长数组。
<pre>
<code>
#include <stdio.h>
void main()
{
	const int length;
    int amount;
	int array[length];
	int i;
	int temp;
	int mid;
	i = 0;
	while(i < length)
	{
		array[i] = i;
		i = i + 1;
    }
    printf("origin array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\nplease input leftshift amount:\n");
    scanf("%d", &amount);
    amount = amount % length;
	mid = (amount - 1) / 2;
	i = 0 ;
	while(i <= mid)
	{
		int new;
		new = amount - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
	mid = (length + amount - 2) / 2;
	i = amount ;
	while(i <= mid)
	{
		int new;
		new = length + amount - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
	mid = (length - 1) / 2;
	i = 0;
	while(i <= mid)
	{
		int new;
		new = length - i - 1;
		temp = array[i];
		array[i] = array[new];
		array[new] = temp;
		i = i + 1;
	}
    printf("the final array is:\n");
    for (i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
	
<code/></pre>

#fibonacci.c文件
用while循环生成fibonacci序列的前20项
		
		设计思路：
		直接按照定义做就可以了
<pre>
<code>
#include <stdio.h>
void main()
{
	const int length;
	int array[length];
	array[0] = 0;
	array[1] = 1;
	int i;
	i = 2;
	while (i < length)
	{
		array[i] = array[i - 1] + array[i - 2];
		i = i + 1;
	}
    printf("the first 20 numbers in fibonacci array:\n");
    for (i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
</code>
</pre>

#sort.c文件
对包含十个数的数组进行排序

		因为c1语言太简陋，所以没有用qsort来排序，而是用了选择排序。选择排序的思路应该不用解释了吧？
		选择排序里会有循环的嵌套
<pre>
<code>
#include <stdio.h>
void main()
{
	int i;
	int j;
	int length;
	const int a[] = {7, 5, 2, 9, 8, 6, 0, 3, 1};
	int val;
	int ind;
	int med;
	length = 9;
	int array[length];
	i = 0;
	while (i < length)
	{
		array[i] = a[i];
		i = i + 1;
	}
	i = 0;
	while (i < length - 1)
	{
		val = array[i];
		ind = i;
		j = i + 1;
		while (j < length)
		{
			if (array[j] < val)
			{
				val = array[j];
				ind = j;
			}
			j = j + 1;
		}
		med = array[ind];
		array[ind] = array[i];
		array[i] = med;
		i = i + 1;
	}
    printf("the origin array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",a[i]);
    printf("\n");
    printf("the sorted array is:\n");
    for(i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
</code>
</pre>
