#include <stdio.h>
#include <string>
#include "node.h"
#define DEBUG

extern int yyparse();
extern int yylex();
extern int stopast;
extern void cleanast();
extern void printnodes();
extern SssNode *root;


int main(int argc, char *argv[])
{
	printf("Parsing ...\n");
	yyparse();
	FILE* dumpfp = fopen("./dump.dot", "w");
	DumpDOT *dumper = new DumpDOT(dumpfp);
	if (stopast)
		;
	else
	{
		root->dumpdot(dumper);
		printnodes();
	}
	delete dumper;
	fclose(dumpfp);
	return(0);
}
