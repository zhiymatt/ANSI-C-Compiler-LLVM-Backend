##查看config/parser.y，以input -> empty | input line为例，说明其AST的结构和构建过程，解释$$、$1、$2、@$等变量的含义（写入文档）

以input -> empty | input line为例，一开始的时候，先通过input -> empty里的动作，创建出InputNode结点。

在输入新的一行后，通过input -> input line这条规则里的动作把line对应的LineNode结点接到InputNode结点中的一个名为lines的vector后面。

之后，在dumpdot()函数中，InputNode和所有的LineNode分别对应graph中一个结点，最后在InputNode和每一个LineNode之间分别连一条线。得到AST中的这一部分。

这部分AST的结构是InputNode为根结点，每一个LineNode都是InputNode的子结点。

$$是产生式左部的非终结符(在例子中是input)的值，$1、$2分别是产生式的第一、第二个token的值，@$是产生式左部非终结符的location，@1、@2分别是产生式的第一、第二个token的location。




##思考AST结点在语法分析中的创建时机和信息填充时机，在P4基础上增添构建AST的代码，保存AST结点对应于源程序的位置信息。
####C1语言要求支持/\*comment*/这样的多行注释，在有多行注释的前提下依然能正确保存位置信息。
####在本节第3点中报告error和warning时需要输出错误位置

匹配到某一条规则之后就在这个规则的动作里创建这个规则对应的结点并填充信息；也有的时候不创建结点，而只是把产生式右侧的值赋给左侧的（因为只要求创建AST，而非分析树）。

我自己自己定义了一个名为update_loc()的函数为YY_USER_ACTION，这个函数一方面把输入保存到一个二维数组里面作为输入缓冲，同时也在不断更新当前token的位置。在update_loc中，碰到\t就把它转换为4个空格并把它保存到输入缓冲里面，同时collum位置加4（我觉得这种方案是比较可行的了。毕竟python规范里也是建议不用tab键而是用四个空格）。其余部分和bison中的差不多。

比较难处理的是多行注释的情况。碰到"/\*"的时候就调用一个自己定义的comment()，comment()函数中采用了一个有限状态机，跳过"/\*"和"*/"之间的注释，同时对注释中的转行符、制表符做特殊处理来跟踪位置信息，更新inbuf二维数组。

之后保存AST结点对应于源程序的位置信息时只要继续用setLoc()就行。
______________
_____
__________
#代码设计思路
lexer.l:
<pre><code>
这个文件里定义了一个名为inbuf[][]的二维数组用来保存输入进来的代码，方便输出错误时指示出错的地方。
还定义了一个update_loc()函数为YY_USER_ACTION，update_loc()会在词法分析的时候保存输入进来的代码、跟踪位置、把输入中的tab键转换为4个空格。
还定义了一个comment()函数，这个函数以有限状态机为模型。当匹配到"/*"时调用这个comment，跳过"/*"和"*/"之间的注释，同时根据注释中的字符数、换行符来跟踪位置。
</code></pre>

parser.y:
<pre><code>
为了在出错后不继续生成AST，我设了一个名为stopast的flag，出错时stopast的值就变成1，当stopast为1的时候就不再创建新的结点。
为了能在出错后释放所有以创建的结点，我设了一个Node*数组，每新创建一个结点就把指向这个结点的指针加入Node*数组中。出错之后就按着这个数组来释放结点。
</code></pre>
node.c:
<pre><code>
因为在左递归的时候要更新结点的last_line和last_column的值（我觉得应该要吧？），所以在每一个append函数里都加了两行代码来更新last_line和last_column的值（不更新也可以，只要把这两行代码注释掉就行）
</code></pre>
要说明思路的基本上就这几个地方。