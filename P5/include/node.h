#ifndef _NODE_HH_
#define _NODE_HH_

#include <stdio.h>
#include <string>
#include <vector>
#include "dumpdot.h"
using namespace std;

typedef enum {
    SSSNODE,//0
    FUNCDEFNODE,//1
    CONSTDEFLISTNODE,//2
    VARLISTNODE,//3
    BLOCKILNODE,//4
    FUNCALLNODE,//5
    ASGNNODE,//6
    IFNODE,//7
    WHILENODE,//8
    IDEXPNODE,//9
    IDNODE,//10
    EXPLISTNODE,//11
    UNARYEXPNODE,//12
    BINARYEXPNODE,//13
    NUMNODE//14
} NodeType;

typedef struct {
    int first_line;
    int first_column;
    int last_line;
    int last_column;
} Loc;

//////////////////////////////////////////////////////////////////

class Node {
public:
    NodeType type;
    Loc* loc;
    Node() 
    {
        loc = (Loc*)malloc(sizeof(Loc));
    }
    void setLoc(Loc* loc);
    //virtual void printast(FILE *fp, int indent) = 0;
    virtual int dumpdot(DumpDOT *dumper) = 0;
};

//////////////////////////////////////////////////////////////////

//expression
class NumNode : public Node {
public:
    int val;
    NumNode(int val) : val(val) 
    { 
        type = NUMNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class BinaryExpNode : public Node {
public:
    char *op;
    Node *lhs, *rhs;
    BinaryExpNode(char *op, Node *lhs, Node *rhs) : op(op), lhs(lhs), rhs(rhs) 
    { 
        type = BINARYEXPNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class UnaryExpNode : public Node {
public:
    char op;
    Node *operand;
    UnaryExpNode(char op, Node *operand) : op(op), operand(operand) 
    { 
        type = UNARYEXPNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class ExplistNode : public Node {
public:
    vector<Node*> exps;
    ExplistNode() 
    { 
        type = EXPLISTNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
    void append(Node *exp);
};

//////////////////////////////////////////////////////////////////

//lval -> id
class IdNode : public Node{
public:
    std::string *name;
    IdNode(std::string* name) : name(name)
    {
        type = IDNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

//lval -> id(exp)
class IdexpNode : public Node {
public:
    std::string *name;
    Node *exp;
    IdexpNode(std::string *name, Node *exp) : name(name), exp(exp)
    {
        type = IDEXPNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

//////////////////////////////////////////////////////////////////

//statement
class WhileNode : public Node {
public:
    BinaryExpNode *cond;
    Node *stmt;
    WhileNode(BinaryExpNode *cond, Node *stmt) : cond(cond), stmt(stmt)
    {
        type = WHILENODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class IfNode : public Node {
public:
    BinaryExpNode *cond;
    Node *stmt1;
    Node *stmt2;
    IfNode(BinaryExpNode *cond, Node *stmt1, 
        Node *stmt2) : cond(cond), stmt1(stmt1), stmt2(stmt2)
    {
        type = IFNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class AsgnNode : public Node {
public:
    Node *lval;
    Node *exp;
    AsgnNode(Node *lval, Node *exp) : lval(lval), exp(exp)
    {
        type = ASGNNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

class FunCallNode : public Node {
public:
    std::string *name;
    FunCallNode(std::string *name) : name(name)
    {
        type = FUNCALLNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

//////////////////////////////////////////////////////////////////

class BlockilNode : public Node {
public:
    vector<Node*> bis;
    BlockilNode() 
    { 
        type = BLOCKILNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
    void append(Node *bi);
};

//////////////////////////////////////////////////////////////////

class VarlistNode : public Node {
public:
    vector<Node*> vars;
    VarlistNode() 
    {
        type = VARLISTNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
    void append(Node* var);
};

//////////////////////////////////////////////////////////////////

class ConstdeflistNode : public Node {
public:
    vector<Node*> cds;
    ConstdeflistNode() 
    {
        type = CONSTDEFLISTNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
    void append(Node* cd);
};

//////////////////////////////////////////////////////////////////

//funcdefnode是函数定义的结点，子结点为id_string和BlockNode
class FuncdefNode : public Node{
public:
    std::string *name;
    BlockilNode *block;
    FuncdefNode(std::string *name, BlockilNode *block) : name(name), block(block)
    {
        type = FUNCDEFNODE;
    }
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
};

//////////////////////////////////////////////////////////////////

//SssNode是根结点，子结点为function的定义(funcdef)和变量的声明(declnode)
class SssNode : public Node {
public:
    vector<Node*> tops;
    SssNode() 
    {
        type = SSSNODE; 
    };
    //void printast(FILE *fp, int indent);
    int dumpdot(DumpDOT *dumper);
    void append(Node* top);
};

//////////////////////////////////////////////////////////////////


#endif
