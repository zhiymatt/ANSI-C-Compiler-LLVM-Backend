这次好像也没必要写shell脚本了<br>
_________________
可以看test文件夹里面的README.md里的说明，看到哪个测试文件有趣就把它重定向输入到compiler程序里面去测试就行了<br>
___________________
先到bin文件夹下的makefile文件进行编译<br>
之后测试<br>
之后运行bin下的view.sh把生成出来的.dot文件转换为png文件<br>
最后执行make clean清理即可
___________
在这里感觉要说明一下控制台里输出的调试信息的含义：

1. blablabla ::= blablablabla 表示使用的产生式
2. 产生式中间会输出一些错误信息，标示错误位置和错误类型
3. 若中途碰到error，就会显示出"delete node %d in %d.%d-%d.%d"，其中第一个％d表示nodetype的enum值（我在nodetype定义的地方把每个结点对应的enum值标出来了），后四个％d表示这个结点在源程序中的位置。之后虽然停止了创建AST，但parse还没有停止，还在继续
4. 最后成功构造AST之后，会输出"we have node %d in %d.%d-%d.%d"，表示现在AST上所有已有的node的类型和位置