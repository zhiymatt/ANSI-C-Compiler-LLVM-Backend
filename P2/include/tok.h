//放在include目录下
#include <string>
using namespace std;

enum token {
    tok_eof = 0,
    tok_num = 1,
    tok_id = 2,
    tok_equalequal = 3,
    tok_exclaimequal = 4,
    tok_lessequal = 5,
    tok_greaterequal = 6,
    tok_plus = 7,
    tok_minus = 8,
    tok_star = 9,
    tok_slash = 10,
    tok_percent = 11,
    tok_l_paren = 12,
    tok_r_paren = 13,
    tok_greater = 14,
    tok_less = 15,
    tok_l_square = 16,
    tok_r_square = 17,
    tok_l_brace = 18,
    tok_r_brace = 19,
    tok_semi = 20,
    tok_comma = 21,
    tok_equal = 22,
    tok_string
};

extern std::string* var_val;
extern int num_val;
