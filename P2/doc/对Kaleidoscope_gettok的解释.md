解释函数gettok()如何向调用者传递token类别、token语义值（数字值、变量名）




#下面是对代码的解释（重点看粗体部分就好）


####这段代码定义了两个全局变量，用来保存当前token的语义值（数字值或变量名）
<pre><code>
static std::string IdentifierStr;  // Filled in if tok_identifier
static double NumVal;              // Filled in if tok_number
</code></pre>

####下面这部分代码通过return语句向调用者传递token的类别
首先下面的代码跳过读到的空格。

		while (isspace(LastChar))
    		LastChar = getchar();

第一种情况：以下面的代码按正则式 "alpha alnum*" 匹配字符串，读到的不是def或extern就说明读到的是identifier。

<pre><code>
  if (isalpha(LastChar)) { // identifier: [a-zA-Z][a-zA-Z0-9]*
    IdentifierStr = LastChar;
    while (isalnum((LastChar = getchar())))
      IdentifierStr += LastChar;

    if (IdentifierStr == "def") return tok_def;
    if (IdentifierStr == "extern") return tok_extern;
    return tok_identifier;
  }
</code></pre>

第二种情况：以下面的代码读取数字串，对应正则式"number*"
<pre><code>
  if (isdigit(LastChar) || LastChar == '.') {   // Number: [0-9.]+
    std::string NumStr;
    do {
      NumStr += LastChar;
      LastChar = getchar();
    } while (isdigit(LastChar) || LastChar == '.');

    NumVal = strtod(NumStr.c_str(), 0);
    return tok_number;
  }
</code></pre>

第三种情况：读到"#",说明这行是注释，可以直接跳过
<pre><code>
  if (LastChar == '#') {
    // Comment until end of line.
    do LastChar = getchar();
    while (LastChar != EOF && LastChar != '\n' && LastChar != '\r');
    
    if (LastChar != EOF)
      return gettok();
  }
</code></pre>

第四种情况：读到eof，返回tok_eof
<pre><code>
if (LastChar == EOF)
    return tok_eof;
</code></pre>

最后，如果和以上四种情况都不符，说明这个tok不是预先定义的def、extern、eof、number...其中之一。