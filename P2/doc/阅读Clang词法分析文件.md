#阅读clang词法分析文件

##Clang - Token.h
###阅读class Token，解释其成员变量的意义（Loc，Kind，FLags，etc），大致理解各个method的功能


>>源代码倒是看懂了，但把自己的解释用中文写出来总感觉词不达意。所以这里用了英文

######各个变量的意义

1. Sourceloation Loc;<br>
the location of the token.

2. unsigned UintData;<br>
the text's length of a nomal token or the end of the SourceRange when encounter a annotation token.

3. void *PtrData;<br>
a union of four different types of pointers:<br>
<1> an IdentifierInfo\* which contain the spelling.<br>
<2> a pointer to the start of the token in the text buffer.<br>
<3> a pointer to sema-specific data for the annotation token.<br>
<4> a pointer to a Decl.<br>
<5> null
4. void *PtrData;

5. tok::TokenKind Kind;<br>
the type of the token.
 
6. unsigned short Flags;<br>
to specify different conditions. For example, if the token is at the start of line, then Flags = StartOfLine; if the identifier contains a UCN, then Flags = hasUCN.

######各个method的含义
1. getKind 返回token的类型
2. is(tok::TokenKind K)	、isNot(tok::TokenKind K)判断token是不是类型K
3. isAnyIdentifier 判断token是不是identifier
4. isLiteral 判断token是不是literal，如string和numeric constant
5. isAnnotation 判断是不是annotation
6. getLocation 得到identifier在文件里的offset位置
7. setLocation 设定id的位置
8. setLength 设定annotation的长度
9. getAnnotationEndLoc 得到annotition结束的位置
10. setAnnotationEndLoc 略
11. getLastLoc 如果是annotation就返回getAnnotationEndLoc，否则返回getLocation
12. getAnnotationRange 返回annotation首尾的位置
13. getName 返回token的名字
14. startToken 把所有flags清零
15. getIdentifierInfo 字面意思
16. setIdentifierInfo
17. getEofData 返回了指向eof的PtrData？不知道干嘛的
18. setEofData 同上
19. getRawIdentifier
20. 。。。。
21. getAnnotationValue 得到annotation的值
22. setAnnotationValue
23. setFlag 添加指定的flag
24. clearFlag 清除指定的flag
25. getFlags 取得flags的值
26. setFlagValue 把flag设为1或0
27. isAtStartOfLine、hasLeadingSpace、isExpandDisabled、isObjCAtKeyword、。。。都是字面意思
  

##Clang - Lexer.h, Lexer.cpp
###从Lex和LexTokenInternal函数开始阅读，重点关注LexTokenInternal中的switch部分
1. 解释Clang如何识别数字（LexNumericConstant）、标识符（LexIdentifier），如何处理＝和＝＝，如何跳过注释（SkipBlockComment，SkipLineComment）<br>
判断数字的方法：

		在LexTokenInternal中，如果token的首字母是[0-9],则说明匹配的有可能是数字，于是调用LexNumericConstant来判断这个token是不是合法，并把LexNumericConstant的返回值作为LexTokenInternal的返回值
		在LexNumericConstant函数中用[a-zA-Z0-9_.]*匹配字符串。若碰到的是1234e+5678这种合法的表达，则匹配完1234e后跳过+，再次调用LexNumbericConstant()来匹配5678后面的字符串，并返回递归调用的值。
		
2. 判断标识符的方法：
	
		在LexTokenInternal中，如果开头的是[A-Za-z_]-[LRUu]，则说明这个token可能是identifier,于是返回LexIdentifier的返回值。
		在LexIdentifier里面，因为之前我们已经匹配过[_A-Za-z$]，我们接着匹配[_A-Za-z0-9]*。当匹配完之后，如果没有碰到"\\","?","$",说明已经到token的最后了。如果现在是在raw mode，则所匹配出来的token即所要的token；否则，还要进行宏展开等后期操作。如果碰到"\\","?","$"，我们就要进入slow path，针对UTF8、UCN等特殊的编码进行处理和判断。
		
3. 如何处理＝和＝＝：
		
		如果当前token的第一个字符是'=‘，则判断第二个字符是不是'='。若不是，则说明是赋值的'='；若是，则还有两种可能，一种可能是'=='，另一种是conflict maker '===='。对于conflict maker，我们一直忽略到行尾就行。
		
4. 如何跳过注释：
		
		《1》跳过/*blablabla*/这种注释：
			若匹配出了‘/*'，就调用SkipBlockComment函数来找到这段注释的结尾。SkipBlockComment是这样工作的：前从前往后找'/'，当找到了之后再看它前一个字符是不是'*'，如果是，这说明已经到了这段注释的结尾，如果不是，则继续找'/'。
		《2》跳过//blablabla这种注释：
			如果第一个字符是'/'，第二个字符也是'/'，则说明这是line comment，这段注释会一直到行尾。这里有个地方要注意，对于"foo //**/ bar", 因为c89没有line comment，所以会翻译成"foo / bar"。而对于有line comment的语言，则会优先匹配"//", 所以这段语句会被翻译成"foo"。因此clang要根据"//"后面是否紧跟着'*‘来做后续判断。
			
###根据以往经验，代码中应尽量避免goto语句，为什么这儿出现了这么多goto？（如果你查看Flex生成的语法分析器，也会看到很多goto）
因为这些代码是用类似于有限状态机的方式来匹配字符串的。goto可以很直观简洁的用于有限状态机中不同状态的转移。另一方面，用goto语句可以让代码更为紧凑、提高匹配字符串的速度（编译器的性能十分关键）。

###识别过程中是如何记录位置信息的？（只要举一种Token的例子）
		lexer在SourceLocation类型的Loc变量中保存,调用其中SourceManager可以得到行号和列号。<br>
		"="为例，当匹配出这个token之后，调用FormTokenWithChars函数来记录位置信息，这个函数会记录token在buffer中处在那一段。
		
###识别出的字符串是如何转换为Token的？Clang是直接用Buffer保存字符串信息还是复制了一个字符串的备份？
		直接用Buffer保存了字符串信息，从FormTokenWithChars函数可以看出来。


##Clang - Diagnostic.h, Diagnostic.cpp 
###了解Diagnostic的作用和原理，设计一个简单的错误程序，用Clang编译它，说明报错流程（你可能要用到Diagnostic.cpp中的FormatDiagnostic函数）



把一个有错的c文件wrong.c用clang编译之后，得到如下的错误信息：

<pre><code>
wrong.c:1:1: warning: return type of 'main' is not 'int' [-Wmain-return-type]
void main()
^
wrong.c:1:1: note: change return type to 'int'
void main()
^~~~
int
wrong.c:18:7: error: expected ';' after expression
        i = 0
             ^
             ;
wrong.c:19:11: warning: equality comparison result unused [-Wunused-comparison]
        while (i == 0, i < length - 1)
               ~~^~~~
wrong.c:19:11: note: use '=' to turn this equality comparison into an assignment
        while (i == 0, i < length - 1)
                 ^~
                 =
wrong.c:23:11: error: expected expression
                j = i /+/ 1;
                        ^
</code></pre>


######diagnostic作用和原理、报错流程
由此可见，clang在编译失败时，会给出错误的严重性级别（error,warning等），错误在源码的位置（插入符号”^”和文件/行/列信息），错误在源码中的范围”~~~~~~”，对应的诊断信息（即出了什么错）。每个诊断信息背后都有一个唯一的ID。</br>

在StoredDiagnostic这个类里面，clang记录了错误的ID、level、location、message、fixhint、range这些信息。

而在DiagnosticsEngine::Report里面，我们可以看到，具体的报错流程是这样的：
1. 把所有的StoredDignostic类型的数据全都加到DiagRanges里<br>
2. 把所有修复的信息都加到DiagFixItHints（用来帮助用户修复错误）<br>
3. 输出在上面两步里面收集起来的所有信息。<br>

######formatdiagnostic()
一个比较关键的函数—— FormatDiagnostic()函数负责把一个diagnostic转换成用来输出的信息。比如，以前面的错误信息为例，一个diagnostic里面有错误的等级error、所在的位置wrong.c:19:11等，而FormatDiagnostic可以把它转换成可以输出到终端的信息：“wrong.c:19:11: error: expected ‘:’ after expression”。

其中，void DiagnosticInfo::FormatDiagnostic(SmallVectorImpl< char > & OutStr ) const 负责把一个diagnostic转换成一个string；<br>		
而void DiagnosticInfo::FormatDiagnostic(const char * DiagStr,const char* DiagEnd, SmallVectorImpl< char > & OutStr)const 负责把arguments嵌入到format string里面。
