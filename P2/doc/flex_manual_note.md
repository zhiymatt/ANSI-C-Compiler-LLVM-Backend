###basic format
		
		definitions
		%%
		rules
		%%
		user code
		
###definition form

		name definition
		
name按照c中定义，在rules中引用时，要在外面加{}, 如{name}, flex会自动替换成(difinition)
<br>example:

         DIGIT    [0-9]
         ID       [a-z][a-z0-9]*
         
用%{ ... %} 来把一整个语句块按原样输出到output。%top{ ... }的效果类似，同时还把 ... 的代码放到顶层
>>即使是在两个％％之间
<br>example1:
		
		 %top{
             /* This code goes at the "top" of the generated file. */
             #include <stdint.h>
             #include <inttypes.h>
         }
<br>example2:
		
		%{
         /* need this for the call to atof() below */
         #include <math.h>
         %}
###rules pattern
		
		pattern action

pattern written in regular expression,

###valid comments
		
	 %{
     /* code block */
     %}
     
     /* Definitions Section */
     %x STATE_X
     
     %%
         /* Rules Section */
     ruleA   /* after regex */ { /* code block */ } /* after code block */
             /* Rules Section (indented) */
     <STATE_X>{
     ruleC   ECHO;
     ruleD   ECHO;
     %{
     /* code block */
     %}
     }
     %%
     /* User Code Section */
     
###patterns
	
		x					匹配字符x
		.					匹配任意字符
		[xyz]				匹配x或y或z
		[abj-oz]
		[^A-Z]				匹配除了大写字母外的字符
		[a-z]{-}[aeiou]		a-z中除了aeiou之外的字符
		r*
		r+
		r?					1个或者0个r
		r{2,5}				2至5个r
		r{2,}				两个以上的r
		r{4}				4个r
		{name}				对name定义的展开
		\\					转义字符\
		\"					转义字符“
		\0					null
		\123				八进制数123的字符
		\x2a				十六进制数2a的字符
		(r)
		‘(?r-s:pattern)’	用参数r、删除参数s来匹配pattern，参数包括i/s/x
							‘i’ means case-insensitive. 
							‘-i’ means case-sensitive.
							‘s’ alters the meaning of the ‘.’ syntax to match any single byte whatsoever. 
							‘-s’ alters the meaning of ‘.’ to match any byte except ‘\n’.
							‘x’ ignores comments and whitespace in patterns. 
								Whitespace is ignored unless it is backslash-escaped, contained 								within ‘""’s, or appears inside a character class.
		(?:foo)         same as  (foo)
     	(?i:ab7)        same as  ([aA][bB]7)
     	(?-i:ab)        same as  (ab)
     	(?s:.)          same as  [\x00-\xFF]
     	(?-s:.)         same as  [^\n]
     	(?ix-s: a . b)  same as  ([Aa][^\n][bB])
     	(?x:a  b)       same as  ("ab")
     	(?x:a\ b)       same as  ("a b")	空格前有\或有[]或有“”则不可忽略该空格
     	(?x:a" "b)      same as  ("a b")
     	(?x:a[ ]b)      same as  ("a b")
     	(?x:a
         	/* comment */
         	b
         	c)          same as  (abc)
		(?# comment )		忽略()里的东西
		rs
		r|s
		r/s					当r后面有s时取r
		^r					只在r是行首时匹配r
		r$					只在r是行尾时匹配r
		<s>r				当在start condition s中时匹配r
		<s1,s2,s3>r			取或
		<*>r				any start conditon
		<<EOF>>
         [:alnum:] [:alpha:] [:blank:]
         [:cntrl:] [:digit:] [:graph:]
         [:lower:] [:print:] [:punct:]
         [:space:] [:upper:] [:xdigit:]
         
         [class A]{-}[class B]		｛－｝计算两个class的差
         [class A]{+}[class B]
		
###匹配方式

当有多个匹配时，选择rules中靠前的匹配<br>

matched后，token的内容被传递到全局变量yytext，长度保存在yyleng里，之后action被执行，之后继续matching<br>

如果没有match，则执行default rule，input被按原样传递到output中<br>

在definitions里面加%pointer或者 %array可以指定yytext的类型<br>
用array会方便修改并可以在外面extern，用pointer比较快<br>

###actions
如果定义了pattern而没有相应的action，则匹配出来的字符串会被舍弃<br>

action里只写一个 ‘｜‘ 则说明执行的动作和下一个规则一样<br>

action里可以用return来向调用yylex()的代码返回值。yylex()会一直执行知道碰到EOF或者return*<br>

在action里面可以用如下的关键字：
		
		ECHO								输出yytext
		BEGIN			
		REJECT								执行到REJECT后把scanner导向下一条能匹配的规则（当前规则里在REJECT的action还是被执行了）（会slow down整个scanner）
		yymore()							把下一次匹配出来的接到这次匹配到的后面
		yyless(n)							把当前token的前n个字符以外的字符回退到inputstream里面
		unput(c)
		input()								若是在c++编译，则用yyinput(),防止重名
		YY_FLUSH_BUFFER						yy_flush_buffer的特例
		YY_INPUT(buf, result, max_size)		把最多max_size的字符读进buf里面，result返回长度活着YY_NULL，及EOF
		yyterminate()
		
###the generated scanner
yylex的定义:	int yylex()<br>

global input file为 yyin<br>

yyrestart(fp)对某个文件重来，这样可以中途改变input stream。但restart不会initialize start condition<br>

当yylex被return中断时，再次调用yylex可以从中断的地方继续（因为用了全局变量）<br>

若YY_INPUT读到EOF，则会调用yywrap()。若yywrap返回0，则设置yyin来继续处理另一个文件；若返回1，则scanner结束，像上层返回0<br>

yywray要自己写<br>

最后，输出到yyout<br>

###start conditions
若rule前面有<conditon>，则当scanner处于名为condition的状态时才会启用这条规则
如
		
		<INITIAL, STRING, QUOTE>\.		{
							blablablab
							}
					
start conditions在definitions里面定义，以％s开头的事inclusive condition，以％x开头的事exclusive condition。在action里面用BEGIN(condition);这样的语句来activate<br>

一次只能有一个start condition被activated，但exclusive condition会使无条件的rules失效，而inclusive condition则不会<br>

<*>用来match所有的conditions<br>

begin(0) 和 begin(INITIAL) 一样<br>

在rules的前面可以用这样的语句, 其中enter_special是全局变量。当yylex()被调用时会执行这语句
		
		%%
			if(enter_special)
				BEGIN(SPECIAL);
				
start conditions的类型是int，用YY_START宏指令取得当前condition的值。也可以用YYSTATE<br>

start condition scope

		<ESC>{
			"\\n"	return '\n';
			"\\r"	return '\r';
		}
		
yy_push_state(int new_state), 把当前状态入栈，之后切换到new_state<br>
yy_pop_state()<br>
yy_top_state()<br>

###multiple input buffers
Function:	YY_BUFFER_STATE yy_create_buffer(FILE *file, int size)	用来创建新的input buffer<br>

Funciton:	void yy_switch_to_buffer(YY_BUFFER_STATE new_buffer)	切换buffer<br>

Function:	void yy_delete_buffer(YY_BUFFER_STATE buffer)<br>
	
Function:	void yypush_buffer_state(YY_BUFFER_STATE buffer)	类似yy_switch_buffer，但会有入栈过程<br>

Function:	void yypop_buffer_state()<br>

Function:	void yy_flush_buffer(YY_BUFFER_STATE buffer)
	把buffer里的内容冲刷掉，下次在scanner再用这个buffer时，scanner会重新用YY_INPUT()往buffer里缓冲<br>
	
Function:	YY_BUFFER_STATE yy_new_buffer(FILE *file, int size)<br>

YY_CURRENT_BUFFER,rval<br>

###miscellaneous macros
\#define YY_USER_ACTION ++ctr[yy_act],	可以自己定义，每次match一条rule的时候都执行一次这个action，<br>

YY_USER_INIT也可以定义。在第一次scan之前执行这个动作<br>

YY_BREAK也可以自己define

##scanner options

##reentrant c scanners
用于多线程时

###serialized tables
用%option tables-file=FILE来产生名为FIlE(默认名为lex.yy.tables)的tables。scanner dependents on this table

##diagonostics
