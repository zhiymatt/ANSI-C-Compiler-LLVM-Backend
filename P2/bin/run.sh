clear

# kaleidoscope

# compile
clang++ -g -O3 ../src/ext_kal.cpp
echo "*******************************************************"
echo kaleidoscope start
# run
./a.out<../test/orfi
# compare
echo -e "\n\n\n"
echo input stream is:
cat ../test/orfi
echo -e "\n\n\n"
echo output stream is:
cat ./tokstream.txt
echo -e "\nkaleiscope end"
echo "*******************************************************"
echo -e "\n\n\n"
#remove
rm tokstream.txt a.out




#flex

cd ..
make
echo -e "\n\n\n"
echo "*******************************************************"
echo c1_lexer start:
echo the output token stream is as follow
echo -e "\n"
./bin/c1c<./test/sort.c
echo -e "\n"
echo c1_lexer ends here
echo "*******************************************************"
echo -e "\n"

make clean
