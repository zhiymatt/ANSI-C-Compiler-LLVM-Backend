#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <string>
#include <vector>
#include <tok.h>

extern FILE *yyin;
extern int yylex();
extern char* yytext;

extern std::string *var_val;
extern int num_val;

void print_token(int token, FILE *stream)
{
    switch(token){
        case tok_id:
            fprintf(stream, " <id, %s> ", yytext);
            break;
        case tok_num:
            fprintf(stream, " <num, %d> ", atoi(yytext));
            break;
        case tok_equalequal:
            fprintf(stream, " <sym, '=='> ");
            break;
        case tok_exclaimequal:
            fprintf(stream, " <sym, '!='> ");
            break;
        case tok_lessequal:
            fprintf(stream, " <sym, '<='> ");
            break;
        case tok_greaterequal:
            fprintf(stream, " <sym, '>='> ");
            break;
        case tok_plus:
            fprintf(stream, " <sym, '+'> ");
            break;
        case tok_minus:
            fprintf(stream, " <sym, '-'> ");
            break;
        case tok_star:
            fprintf(stream, " <sym, '*'> ");
            break;
        case tok_slash:
            fprintf(stream, " <sym, '/'> ");
            break;
        case tok_percent:
            fprintf(stream, " <sym, '%%'> ");
            break;
        case tok_l_paren:
            fprintf(stream, " <sym, '('> ");
            break;
        case tok_r_paren:
            fprintf(stream, " <sym, ')'> ");
            break;
        case tok_greater:
            fprintf(stream, " <sym, '>'> ");
            break;
        case tok_less:
            fprintf(stream, " <sym, '<'> ");
            break;
        case tok_l_square:
            fprintf(stream, " <sym, '['> ");
            break;
        case tok_r_square:
            fprintf(stream, " <sym, ']'> ");
            break;
        case tok_l_brace:
            fprintf(stream, " <sym, '{'> ");
            break;
        case tok_r_brace:
            fprintf(stream, " <sym, '}'> ");
            break;
        case tok_semi:
            fprintf(stream, " <sym, ';'> ");
            break;
        case tok_comma:
            fprintf(stream, " <sym, ','> ");
            break;
        case tok_equal:
            fprintf(stream, " <sym, '='> ");
            break;
        case tok_string:
            fprintf(stream, "blablabla");
            break;
        default:;
    }
    return;
}


int main(){
    FILE *fp = fopen("hey.c","r");
    yyin = fp;
    int token;
    while(token = yylex())
        print_token(token, stdout);
    return 0;
}
