#0. 运行/bin文件夹下的run.sh文件来执行shell脚本，用来检查修改后的kaleidoscope和c1_lexer

<br><br>
#1. 理解kaleidoscope的词法分析过程
kaleidoscope部分的文档在/doc文件夹下,<br>
而修改后的源代码为/src文件夹下的ext_kal.cpp文件<br>
/test文件夹下的orfi文件用来测试

#2. 学习使用Flex
笔记也在/doc文件夹下面

#3. 用Flex生成C1的词法分析器
这一部分包括<br>
1. /config/lexer.l<br>
2. /include/tok.h<br>
3. /src/main.cpp<br>
4. /src/tok.cpp<br>
test文件夹下的sort.c文件用来测试

#4. 阅读Clang词法分析文件
clang部分的文档也在/doc文件夹下