#include <stdio.h>
void main()
{
	const int length = 20;
	int array[length];
	array[0] = 0;
	array[1] = 1;
	int i;
	i = 2;
	while (i < length)
	{
		array[i] = array[i - 1] + array[i - 2];
		i = i + 1;
	}
    printf("the first 20 numbers in fibonacci array:\n");
    for (i = 0;i < length;i++)
        printf("%d ",array[i]);
    printf("\n");
}
