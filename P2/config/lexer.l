%{
    #include <tok.h>
    #include <stdio.h>
    #include <stdlib.h>
%}

%x  comment1
%x  comment2
%x  stringmode

id  [a-zA-Z_][a-zA-Z_0-9]*
dig [0-9]
ws  [ \t]



%%


%{
	int line_num = 1;
%}


{id} 			    return tok_id;


[+-]?{dig}+         return tok_num;


"=""="              return tok_equalequal;
"!""="              return tok_exclaimequal;
"<""="              return tok_lessequal;
">""="              return tok_greaterequal;
"+"                 return tok_plus;
"-"                 return tok_minus;
"*"                 return tok_star;
"/"                 return tok_slash;
"%"                 return tok_percent;
"("                 return tok_l_paren;
")"                 return tok_r_paren;
">"                 return tok_greater;
"<"                 return tok_less;
"["                 return tok_l_square;
"]"                 return tok_r_square;
"{"                 return tok_l_brace;
"}"                 return tok_r_brace;
";"                 return tok_semi;
","                 return tok_comma;
"="                 return tok_equal;


{ws}                {}


\\n                 ++line_num;


"/""*"              BEGIN(comment1);
<comment1>{
    [^*\n]*         {}
    "*"+[^*/\n]*    {}
    \n              ++line_num;
    "*"+"/"         BEGIN(INITIAL);
}


"/""/"              BEGIN(comment2);
<comment2>\n        {
                        ++line_num;
                        BEGIN(INITIAL);
                    }


"\""                BEGIN(stringmode);
<stringmode>"\""    {
                        return tok_string;
                        BEGIN(INITIAL);
                    }


<<EOF>>           {return tok_eof;}


%%









int yywrap(){
	return 1;
}
