#test_basic.c
基本功能的测试。<br>
测试全局数组、局部数组、全局变量、局部变量、循环嵌套、if－else语句、作用域、变量定义等。<br>
可以看具体代码来检查。<br>

#test_inout.c
测试静态库的scan()和print(),<br>
代码实现了把输入再输出出来的功能。<br>
#test_fibonacci.c
计算前二十个费波那契数

#test_mdstack.c
输入两个数字，计算它们的最大公倍数和最小公约数

#test_recursion.c
递归的计算输入的数字的阶乘（数字太大可能会溢出）

#test_sort.c
输入10个数字，然后对这10个数字进行计算

#test_conbre.c
测试break语句和continue语句

