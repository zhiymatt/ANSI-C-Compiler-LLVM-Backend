
int varrr = 000002 - 1;
int i[3] = {111111, 111112, 111113};

void func()
{
	Output = 333331;
	print();

}

void main()
{
	//test global var
	Output = varrr;
	print();
	//test global array
	Output = i[0];
	print();
	Output = i[1];
	print();
	Output = i[2];
	print();
	//test local array
	int i[3] = {222221, 222222, 222223};
	Output = i[0];
	print();
	Output = i[1];
	print();
	Output = i[2];
	print();
	//test declare var
	int j = 1;
	int i = 2 + j, k = 10;
	const int const100 = 100;
	//test scope between function
	func();
	Output = 333332;
	print();
	func();
	i = 0;
	//test while loop and if-else statement
	while (i < 10)
	{
		if (i >= 5)
		{
			Output = 444442;
			print();
		}
		else
		{
			int j = 0;
			//test nested while loop
			while (j < 3)
			{	
				Output = 444441;
				print();
				j = j + 1;
			}
		}
		i = i + 1;
	}
	//test scope in different scope
	int a = 555551;
	Output = a;
	print();
	{
		int a = 555552;
		Output = a;
		print();
		{
			int a = 555553;
			Output = a;
			print();
		}
		Output = a;
		print();
	}
	Output = a;
	print();
	//test const declaration
	const int conval = 6661;
	Output = conval;
	print();
}
