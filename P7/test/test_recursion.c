int stack[100];
int top, pushvalue, popvalue;

void pop()
{
    popvalue = stack[top];
    top = top - 1;
}
void push()
{
    top = top + 1;
    stack[top] = pushvalue;
}

void factorial()
{
    int n;
    pop();
    n = popvalue;
    if(n == 1)
        pushvalue = 1;
    else {
        pushvalue = n - 1;
        push();
        factorial();
        pop();
        pushvalue = popvalue * n;
    }
    push();
}

void main()
{
    int result;
    scan();
    pushvalue = Input;
    push();
    factorial();
    pop();
    result = popvalue;
    Output = result;
    print();
}
