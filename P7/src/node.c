#include "node.h"
#include "util.h"

void Node::setLoc(Loc* loc) 
{
    this->loc->first_line   = loc->first_line;
    this->loc->first_column = loc->first_column;
    this->loc->last_line    = loc->last_line;
    this->loc->last_column  = loc->last_column;
}

void SssNode::append(Node *top) 
{
    tops.push_back(top);
    this->loc->last_line = top->loc->last_line;
    this->loc->last_column = top->loc->last_column;
}

void ConstlistNode::append(ConstNode *cd) 
{
    cds.push_back(cd);
    this->loc->last_line = cd->loc->last_line;
    this->loc->last_column = cd->loc->last_column;
}

void VarlistNode::append(VarNode *var) 
{
    vars.push_back(var);
    this->loc->last_line = var->loc->last_line;
    this->loc->last_column = var->loc->last_column;
}

void BlockilNode::append(Node *bi) 
{
    bis.push_back(bi);
    this->loc->last_line = bi->loc->last_line;
    this->loc->last_column = bi->loc->last_column;
}

void ExplistNode::append(ExpNode *exp) 
{
    exps.push_back(exp);
    this->loc->last_line = exp->loc->last_line;
    this->loc->last_column = exp->loc->last_column;
}
