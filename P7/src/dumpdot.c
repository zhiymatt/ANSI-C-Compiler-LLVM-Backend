#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <map>
#include <string>
#include <vector>
#include <sstream>
#include "node.h"
#include "dumpdot.h"

//===----------------------------------------------------------------------===//
// Utilities in Dump DOT
//===----------------------------------------------------------------------===//

// There are two ways to create a dot node:
// 1. newNode(num, str1, str2, ...)
//    num corresponds to the number of strings
//    Each string will appear in the generated image as a port
//    All strings are char*
// 2. newNode(vec)
//    All elements of the vector are std::string
// newNode returns an integer, which is the number of the corresponding
// node in DOT file.

int DumpDOT::newNode(int num, ...) {
    va_list list;
    va_start(list, num);
    fprintf(fp, "    %d [label = \"", count);
    bool first = true;
    for (int i=0; i<num; i++) {
        char* st = va_arg(list, char*);
        if (!first)
            fprintf(fp, "|");
        first = false;
        if (st[0]=='<')
            fprintf(fp, "<%d> \\<", i);
        else
            fprintf(fp, "<%d> %s", i, st);
    }
    va_end(list);
    fprintf(fp, "\"];\n");
    return count++;
}

int DumpDOT::newNode(std::vector<std::string> list) {
    fprintf(fp, "    %d [label = \"", count);
    bool first = true;
    for (int i=0; i<list.size(); i++) {
        std::string st = list[i];
        if (!first)
            fprintf(fp, "|");
        first = false;
        fprintf(fp, "<%d> %s", i, st.c_str());
    }
    fprintf(fp, "\"];\n");
    return count++;
}

// Draw a line from nSrc node's pSrc port to nDst node.
// If you want it start from the whole node, let pSrc = -1

void DumpDOT::drawLine(int nSrc, int pSrc, int nDst) {
    fprintf(fp, "    %d", nSrc);
    if (pSrc>=0)
        fprintf(fp, ":%d", pSrc);
    fprintf(fp, " -> %d;\n", nDst);
}

//===----------------------------------------------------------------------===//
// Dump AST to DOT
//===----------------------------------------------------------------------===//

// The following functions convert AST to DOT using DumpDOT.
// Each dumpdot returns an integer, which is corresponding number in DOT file.
// 53+29*71 will become:
// digraph {
// node [shape = record];
//     0 [label = "<0> |<1> +|<2> "];
//     1 [label = "<0> 53"];
//     2 [label = "<0> |<1> *|<2> "];
//     3 [label = "<0> 29"];
//     4 [label = "<0> 71"];
//     0:0 -> 1;
//     0:2 -> 2;
//     2:0 -> 3;
//     2:2 -> 4;
// }


int NumNode::dumpdot(DumpDOT *dumper) {
    std::ostringstream strs;
    strs << val;
    int nThis = dumper->newNode(1, strs.str().c_str());
    return nThis;
}

int BinaryExpNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(3, " ", op, " ");
    int nlhs = lhs->dumpdot(dumper);
    int nrhs = rhs->dumpdot(dumper);
    dumper->drawLine(nThis, 0, nlhs);
    dumper->drawLine(nThis, 2, nrhs);
    return nThis;
}

int UnaryExpNode::dumpdot(DumpDOT *dumper) {
    char st[2] = " ";
    st[0] = op;
    int nThis = dumper->newNode(2, st, " ");
    int nOperand = operand->dumpdot(dumper);
    dumper->drawLine(nThis, 1, nOperand);
    return nThis;
}

int ExplistNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "exp list");
    for (ExpNode* expr : exps) 
    {
        int nExp = expr->dumpdot(dumper);
        dumper->drawLine(nThis, 0, nExp);
    }
    return nThis;
}

int IdNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, name->c_str());
    return nThis;
}

int IdexpNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(2, name->c_str(), "[ ]");
    if (expr != NULL)
    {
        int nExp = expr->dumpdot(dumper);
        dumper->drawLine(nThis, 1, nExp);
    }
    return nThis;
}

int WhileNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(4, "while", " ", "do", " ");
    int nCond = cond->dumpdot(dumper);
    int nStmt = stmt->dumpdot(dumper);
    dumper->drawLine(nThis, 1, nCond);
    dumper->drawLine(nThis, 3, nStmt);
    return nThis;
}

int IfNode::dumpdot(DumpDOT *dumper) {
    int nThis;
    if (stmt2 == NULL)
    {    
        nThis = dumper->newNode(4, "if", " ", "then", " ");
    }
    else
    {
        nThis = dumper->newNode(6, "if", " ", "then", " ", "else", " ");
        int nStmt2 = stmt2->dumpdot(dumper);
        dumper->drawLine(nThis, 5, nStmt2);
    }
    int nCond = cond->dumpdot(dumper);
    int nStmt1 = stmt1->dumpdot(dumper);
    dumper->drawLine(nThis, 1, nCond);
    dumper->drawLine(nThis, 3, nStmt1);
    return nThis;
}

int AsgnNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(3, " ","=", " ");
    int nLval = lval->dumpdot(dumper);
    int nExp = expr->dumpdot(dumper);
    dumper->drawLine(nThis, 0, nLval);
    dumper->drawLine(nThis, 2, nExp);
    return nThis;
}

int FunCallNode::dumpdot(DumpDOT *dumper) {
    std::ostringstream strs;
    strs << "call func " << name->c_str() << "()";
    int nThis = dumper->newNode(1, strs.str().c_str());
    return nThis;
}

int BlockilNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "block items");
    for (Node* bi : bis)
    {
        int nBi = bi->dumpdot(dumper);
        dumper->drawLine(nThis, 0, nBi);
    }
    return nThis;
}

int VarlistNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "declare vars");
    for (VarNode* var : vars)
    {
        int nVar = var->dumpdot(dumper);
        dumper->drawLine(nThis, 0, nVar);
    }
    return nThis;
}

int Var1Node::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(2, name->c_str(), "=");
    if (expr != NULL)
    {
        int nExp = expr->dumpdot(dumper);
        dumper->drawLine(nThis, 1, nExp);
    }
    return nThis;
}

int VarArrNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(3, name->c_str(), "[]", "= {...}");
    if (expr != NULL)
    {
        int nExp = expr->dumpdot(dumper);
        dumper->drawLine(nThis, 1, nExp);
    }
    if (explist != NULL)
    {
        int nExplist = explist->dumpdot(dumper);
        dumper->drawLine(nThis, 2, nExplist);
    }
    return nThis;
}

int ConstlistNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "const declare vars");
    for (Node* cd : cds)
    {
        int nCd = cd->dumpdot(dumper);
        dumper->drawLine(nThis, 0, nCd);
    }
    return nThis;
}

int Const1Node::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(2, name->c_str(), "=");
    int nExp = expr->dumpdot(dumper);
    dumper->drawLine(nThis, 1, nExp);
    return nThis;
}

int ConstArrNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(3, name->c_str(), "[]", "= {...}");
    if (expr != NULL)
    {
        int nExp = expr->dumpdot(dumper);
        dumper->drawLine(nThis, 1, nExp);
    }
    int nExplist = explist->dumpdot(dumper);
    dumper->drawLine(nThis, 2, nExplist);
    return nThis;
}

int FuncdefNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(3, "function", name->c_str(), "block");
    int nBlock = block->dumpdot(dumper);
    dumper->drawLine(nThis, 2, nBlock);
    return nThis;
}

int SssNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "START!");
    for (Node* top : tops) 
    {
        int nTop = top->dumpdot(dumper);
        dumper->drawLine(nThis, 0, nTop);
    }
    return nThis;
}

int BreakNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "break");
    return nThis;
}

int ContinueNode::dumpdot(DumpDOT *dumper) {
    int nThis = dumper->newNode(1, "continue");
    return nThis;
}
