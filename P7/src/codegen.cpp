#include "node.h"
#include <llvm/IR/Module.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/PassManager.h>
#include <llvm/Transforms/Scalar.h>
#include "llvm/IR/Verifier.h"

#include <cstdio>
#include <iostream>
#include <string.h>

using namespace llvm;

Module*TheModule;
IRBuilder<> Builder(getGlobalContext());
std::map<std::string, AllocaInst *> NamedValues;

GlobalVariable* Output;
GlobalVariable* Input;
Function *func_print;
Function *func_scan;
FunctionPassManager FPM(TheModule);

static int isglobal = 1;
Value* zero;
BasicBlock *LastCondBB;
BasicBlock *LastAfterBB;

void init()
{
	//init module
	TheModule = new Module("demo", getGlobalContext());
    FPM.add(createPromoteMemoryToRegisterPass());
    FPM.doInitialization();
    //init lib var and function
    Output = new GlobalVariable(*TheModule, IntegerType::get(getGlobalContext(), 32),
			false, GlobalValue::ExternalLinkage, 0, "Output");
    Input = new GlobalVariable(*TheModule, IntegerType::get(getGlobalContext(), 32),
			false, GlobalValue::ExternalLinkage, 0, "Input");
    NamedValues["Input"] = (AllocaInst*)Input;
    std::vector<Type*> func_args;
	FunctionType *func_type = 
		FunctionType::get(Type::getVoidTy(getGlobalContext()),
			func_args, true);
	func_print = Function::Create(func_type,
		GlobalValue::ExternalLinkage, "print", TheModule);
	func_scan = Function::Create(func_type,
		GlobalValue::ExternalLinkage, "scan", TheModule);
	zero = ConstantInt::get(TheModule->getContext(), APInt(32, 0, 10));
}

void go()
{
	TheModule->dump();
}

//check
Value *NumNode::Codegen() {
	return ConstantInt::get(TheModule->getContext(), APInt(32, val));
} 

//check
Value *BinaryExpNode::Codegen() {
	Value *L = lhs->Codegen();
	Value *R = rhs->Codegen();
	switch (*op) {
		case '+':
	    	return Builder.CreateAdd(L, R, "addtmp");
		case '-':
			return Builder.CreateSub(L, R, "subtmp");
		case '*':
	  		return Builder.CreateMul(L, R, "multmp");
		case '/':
			return Builder.CreateSDiv(L, R, "divtmp");
		case '%':
			return Builder.CreateSRem(L, R, "remtmp");
		default:	//'==' | '!=' | '<' | '>' | '<=' | '>='
		{
			if (strcmp(op, ">") == 0)
				return Builder.CreateICmpSGT(L, R, "gttmp");
			else if (strcmp(op, "<") == 0)
				return Builder.CreateICmpSLT(L, R, "lttmp");
			else if (strcmp(op, "==") == 0)
				return Builder.CreateICmpEQ(L, R, "eqtmp");
			else if (strcmp(op, "!=") == 0)
				return Builder.CreateICmpNE(L, R, "netmp");
			else if (strcmp(op, "<=") == 0)
				return Builder.CreateICmpSLE(L, R, "letmp");
			else if (strcmp(op, ">=") == 0)
				return Builder.CreateICmpSGE(L, R, "getmp");
			else
				return 0;
		}
	}
}

//check
Value *UnaryExpNode::Codegen() {
	Value *operandV = operand->Codegen();
	if (op == '-')
	{
		Value *zeroV = ConstantInt::get(TheModule->getContext(), APInt(32, 0));
		return Builder.CreateSub(zeroV, operandV, "minustmp");
	}
	else
		return operandV;
}

//check
Value *ExplistNode::Codegen(){
    for (ExpNode* expr : exps) 
    {
        Value *value = expr->Codegen();
    }
    return 0;
}

//check
Value *IdNode::Codegen(){
	Value *Variable = Builder.CreateLoad(NamedValues[*name], "lval");
	return Variable;
}

//check
Value *IdexpNode::Codegen(){
	//fix me: support array
	Value* Alloca = NamedValues[*name];
	Value* index[] = {zero, expr->Codegen()};
	Value* place = Builder.CreateInBoundsGEP(Alloca, index, "place");
	return Builder.CreateLoad(place);
}

//check
Value *WhileNode::Codegen(){
	Function *TheFunction = Builder.GetInsertBlock()->getParent();
	BasicBlock *CondBB = BasicBlock::Create(getGlobalContext(), "cond", TheFunction);
	BasicBlock *LoopBB = BasicBlock::Create(getGlobalContext(), "loop", TheFunction);
	BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "after", TheFunction);
	BasicBlock *tmpLastCondBB = LastCondBB;
	BasicBlock *tmpLastAfterBB = LastAfterBB;
	LastCondBB = CondBB;
	LastAfterBB = AfterBB;
	Builder.CreateBr(CondBB);
	Builder.SetInsertPoint(CondBB);
	Value *CondVal = cond->Codegen();
	Builder.CreateCondBr(CondVal, LoopBB, AfterBB);
	Builder.SetInsertPoint(LoopBB);
	Value *LoopVal = stmt->Codegen();
	Builder.CreateBr(CondBB);
	Builder.SetInsertPoint(AfterBB);
	LastCondBB = tmpLastCondBB;
	LastAfterBB = tmpLastAfterBB;
	return 0;
}

Value *BreakNode::Codegen(){
	Builder.CreateBr(LastAfterBB);
	return 0;
}

Value *ContinueNode::Codegen(){
	Builder.CreateBr(LastCondBB);
	return 0;
}

//check
Value *IfNode::Codegen(){
	Function *TheFunction = Builder.GetInsertBlock()->getParent();
	BasicBlock *EntryBB = BasicBlock::Create(getGlobalContext(), "entry", TheFunction);
	BasicBlock *TrueBB = BasicBlock::Create(getGlobalContext(), "true", TheFunction);
	BasicBlock *FalseBB;
	BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "after", TheFunction);
	Builder.CreateBr(EntryBB);
	Builder.SetInsertPoint(EntryBB);
	Value *CondV = cond->Codegen();
	if (stmt2 != NULL)
	{
		FalseBB = BasicBlock::Create(getGlobalContext(), "false", TheFunction);
		Builder.CreateCondBr(CondV, TrueBB, FalseBB);
	}
	else 
		Builder.CreateCondBr(CondV, TrueBB, AfterBB);
	Builder.SetInsertPoint(TrueBB);
	Value *TrueV = stmt1->Codegen();
	Builder.CreateBr(AfterBB);
	if (stmt2 != NULL) {
		Builder.SetInsertPoint(FalseBB);
		Value *FalseV = stmt2->Codegen();
		Builder.CreateBr(AfterBB);
	}
	Builder.SetInsertPoint(AfterBB);
	return 0;
}

//check
Value *AsgnNode::Codegen(){
    Value *expV = expr->Codegen();
    if (strcmp(lval->getName()->c_str(),"Output") == 0)
    {
    	Builder.CreateStore(expV, Output);
    	return expV;
    }
    AllocaInst* place = NamedValues[*(lval->getName())];
    if (lval->type == IDEXPNODE)
    {
		Value* index[] = {zero, ((IdexpNode*)lval)->expr->Codegen()};
		place = (AllocaInst*)Builder.CreateInBoundsGEP(place, index, "place");
    }
	Builder.CreateStore(expV, place);
    return expV;
}

//check
Value *FunCallNode::Codegen(){
	if (strcmp(name->c_str(), "scan") == 0)
	{
		Builder.CreateCall(func_scan);
		return 0;
	}
	if (strcmp(name->c_str(), "print") == 0)
	{
		Builder.CreateCall(func_print);
		return 0;
	}
	std::vector<Value *> ArgsV;
	Function *CalleeF = TheModule->getFunction(*name);
	if (!CalleeF) {
		std::cout << *name << std::endl;
		return 0;
	}
 	return Builder.CreateCall(CalleeF, ArgsV);
}

//check
Value *BlockilNode::Codegen(){
	std::map<std::string, AllocaInst *> tmpNamedValues;
	tmpNamedValues = NamedValues;
    for (Node* bi : bis) 
    {
        Value *value = bi->Codegen();
    }
    NamedValues = tmpNamedValues;
    return 0;
}

//check
Value *Var1Node::Codegen(){
	if (isglobal)
	{
		GlobalVariable* gvalue = new GlobalVariable(
			*TheModule, IntegerType::get(getGlobalContext(), 32), false,
				GlobalValue::ExternalLinkage,
					0, "gvalue"); 
		gvalue->setAlignment(4);
		NamedValues[*name] = (AllocaInst*)gvalue;
		ConstantInt* initv;
		if (expr != NULL)
			initv = ConstantInt::get(TheModule->getContext(), APInt(32, expr->calconst(), 10));
		else
			initv = ConstantInt::get(TheModule->getContext(), APInt(32, 0, 10));
		gvalue->setInitializer(initv);
		return 0;
	}
	Value *initV;
	if (expr)
		initV = expr->Codegen();
	else
		initV = ConstantInt::get(TheModule->getContext(), APInt(32, 0));
	AllocaInst *Alloca = 
		Builder.CreateAlloca(IntegerType::get(TheModule->getContext(), 32), initV, "var");
	Alloca->setAlignment(4);
	Builder.CreateStore(initV, Alloca);
	NamedValues[*name] = Alloca;
	return 0;
}

//check
Value *VarArrNode::Codegen(){
	int size = expr->calconst();
	ArrayType *ArrayTy = ArrayType::get(
		IntegerType::get(TheModule->getContext(), 32), size);
	if (!isglobal) {
		int size = expr->calconst();
		AllocaInst *Alloca = Builder.CreateAlloca(ArrayTy, zero);
		Alloca->setAlignment(4);
		NamedValues[*name] = Alloca;
		if(explist != NULL)
		{
			int i = 0;
			int v;
			Value* place;
			for (ExpNode* expr : explist->exps) 
	    	{ 
	        	v = expr->calconst();
	        	Value* place;
	        	Value* index_vector[] = {zero, 
	        		ConstantInt::get(TheModule->getContext(), APInt(32, i, 10))}; 
	        	place = Builder.CreateInBoundsGEP(
	        		Alloca, index_vector, "place"); 
				Builder.CreateStore(ConstantInt::get(TheModule->getContext(), APInt(32, v, 10)), place);
	    		i++;
			}
		}	     	
		return 0;
	} 
	GlobalVariable* gvalue = new GlobalVariable(
			*TheModule, ArrayTy, false,
				GlobalValue::ExternalLinkage,
					0, "gvalue"); 
	gvalue->setAlignment(16);
	std::vector<Constant*> initv;
	if(explist != NULL)
	{
		for (ExpNode* expr : explist->exps)
		{
			ConstantInt* v = ConstantInt::get(
				TheModule->getContext(), APInt(32, expr->calconst(), 10)); 
			initv.push_back(v);
		}
	}
	else
	{
		{
			ConstantInt* v = ConstantInt::get(
				TheModule->getContext(), APInt(32, 0, 10)); 
			initv.push_back(v);
		}
	} 
	Constant* initvarray = ConstantArray::get(ArrayTy, initv);
	gvalue->setInitializer(initvarray);
	NamedValues[*name] = (AllocaInst*)gvalue;
	return 0;
}


//check
Value *VarlistNode::Codegen(){ 
	for (VarNode* var : vars)
		Value *value = var->Codegen();
	return 0;
}

//check
Value *Const1Node::Codegen(){	
	if (isglobal)
	{
		GlobalVariable* gvalue = new GlobalVariable(
			*TheModule, IntegerType::get(getGlobalContext(), 32), false,
				GlobalValue::ExternalLinkage,
					0, "gvalue"); 
		gvalue->setAlignment(4);
		NamedValues[*name] = (AllocaInst*)gvalue;
		ConstantInt* initv;
		if (expr != NULL)
			initv = ConstantInt::get(TheModule->getContext(), APInt(32, expr->calconst(), 10));
		else
			initv = ConstantInt::get(TheModule->getContext(), APInt(32, 0, 10));
		gvalue->setInitializer(initv);
		return 0;
	}
	Value *initV;
	if (expr)
		initV = expr->Codegen();
	else
		initV = ConstantInt::get(TheModule->getContext(), APInt(32, 0));
	AllocaInst *Alloca = 
		Builder.CreateAlloca(IntegerType::get(TheModule->getContext(), 32), initV, "var");
	Alloca->setAlignment(4);
	Builder.CreateStore(initV, Alloca);
	NamedValues[*name] = Alloca;
	return 0;
}

//check
Value *ConstArrNode::Codegen(){	
	int size = expr->calconst();
	ArrayType *ArrayTy = ArrayType::get(
		IntegerType::get(TheModule->getContext(), 32), size);
	if (!isglobal) {
		int size = expr->calconst();
		AllocaInst *Alloca = Builder.CreateAlloca(ArrayTy, zero);
		Alloca->setAlignment(4);
		NamedValues[*name] = Alloca;
		if(explist != NULL)
		{
			int i = 0;
			int v;
			Value* place;
			for (ExpNode* expr : explist->exps) 
	    	{ 
	        	v = expr->calconst();
	        	Value* place;
	        	Value* index_vector[] = {zero, 
	        		ConstantInt::get(TheModule->getContext(), APInt(32, i, 10))}; 
	        	place = Builder.CreateInBoundsGEP(
	        		Alloca, index_vector, "place"); 
				Builder.CreateStore(ConstantInt::get(TheModule->getContext(), APInt(32, v, 10)), place);
	    		i++;
			}
		}	     	
		return 0;
	} 
	GlobalVariable* gvalue = new GlobalVariable(
			*TheModule, ArrayTy, false,
				GlobalValue::ExternalLinkage,
					0, "gvalue"); 
	gvalue->setAlignment(16);
	std::vector<Constant*> initv;
	if(explist != NULL)
	{
		for (ExpNode* expr : explist->exps)
		{
			ConstantInt* v = ConstantInt::get(
				TheModule->getContext(), APInt(32, expr->calconst(), 10)); 
			initv.push_back(v);
		}
	}
	else
	{
		{
			ConstantInt* v = ConstantInt::get(
				TheModule->getContext(), APInt(32, 0, 10)); 
			initv.push_back(v);
		}
	} 
	Constant* initvarray = ConstantArray::get(ArrayTy, initv);
	gvalue->setInitializer(initvarray);
	NamedValues[*name] = (AllocaInst*)gvalue;
	return 0;
}

//check
Value *ConstlistNode::Codegen(){ 
	for (ConstNode* cd : cds)
		Value *value = cd->Codegen();
	return 0;
}

//check	
Value *FuncdefNode::Codegen(){
	isglobal = 0;
	std::vector<Type*> FuncTy_args;
	FunctionType* FuncTy = FunctionType::get(
		Type::getVoidTy(TheModule->getContext()),
  		FuncTy_args, false);
	Function* func = Function::Create(
		FuncTy, Function::ExternalLinkage, *name, TheModule); 
	BasicBlock* BB = BasicBlock::Create(getGlobalContext(), "entry", func);
	Builder.SetInsertPoint(BB);
  	block->Codegen();	
	Builder.CreateRet(0);
	FPM.run(*func);
	isglobal = 1;
	return 0;
}

//check
Value *SssNode::Codegen(){
	init();
	for (Node* top : tops)
	{
		Value* value = top->Codegen();
	}
	go();
	return 0;
}
