#ifndef _NODE_HH_
#define _NODE_HH_

#include <stdio.h>
#include <string>
#include <string.h>
#include <vector>
#include <map>
#include "dumpdot.h"

#include "llvm/IR/Value.h"
using namespace llvm;

typedef enum {
    NUMNODE,//0
    BINARYEXPNODE,//1
    UNARYEXPNODE,//2
    EXPLISTNODE,//3
    IDNODE,//4
    IDEXPNODE,//5
    WHILENODE,//6
    IFNODE,//7
    ASGNNODE,//8
    FUNCALLNODE,//9
    BLOCKILNODE,//10
    VAR1NODE,//11
    VARARRNODE,//12
    VARLISTNODE,//15
    CONST1NODE,//16
    CONSTARRNODE,//17
    CONSTLISTNODE,//18
    FUNCDEFNODE,//19
    SSSNODE//20
} NodeType;

typedef struct {
    int first_line;
    int first_column;
    int last_line;
    int last_column;
} Loc;

//////////////////////////////////////////////////////////////////

static std::map<std::string, int> consttable;

//////////////////////////////////////////////////////////////////
class Node {
public:
    NodeType type;
    Loc* loc;
    Node() 
    {
        loc = (Loc*)malloc(sizeof(Loc));
    }
    void setLoc(Loc* loc);
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual ~Node(){}
};

//////////////////////////////////////////////////////////////////

class ExpNode : public Node {
public: 
    virtual ~ExpNode() {}
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual int calconst() = 0;
};

class NumNode : public ExpNode {
public:
    int val;
    NumNode(int val) : val(val) 
    { 
        type = NUMNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
    int calconst()
    {
        return val;
    }
};

class BinaryExpNode : public ExpNode {
public:
    char *op;
    ExpNode *lhs, *rhs;
    BinaryExpNode(char *op, ExpNode *lhs, ExpNode *rhs) : op(op), lhs(lhs), rhs(rhs) 
    { 
        type = BINARYEXPNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
    int calconst()
    {
        switch (*op) {
            case '+':
                return lhs->calconst() + rhs->calconst();
            case '-':
                return lhs->calconst() - rhs->calconst();
            case '*':
                return lhs->calconst() * rhs->calconst();
            case '/':
                return lhs->calconst() / rhs->calconst();
            case '%':
                return lhs->calconst() % rhs->calconst();
            default:    //'==' | '!=' | '<' | '>' | '<=' | '>='
            {
                if (strcmp(op, ">") == 0)
                    return lhs->calconst() > rhs->calconst();
                else if (strcmp(op, "<") == 0)
                    return lhs->calconst() < rhs->calconst();
                else if (strcmp(op, "==") == 0)
                    return lhs->calconst() == rhs->calconst();
                else if (strcmp(op, "!=") == 0)
                    return lhs->calconst() != rhs->calconst();
                else if (strcmp(op, "<=") == 0)
                    return lhs->calconst() <= rhs->calconst();
                else if (strcmp(op, ">=") == 0)
                    return lhs->calconst() >= rhs->calconst();
                else
                    return 0;
            }
        }
    }
};

class UnaryExpNode : public ExpNode {
public:
    char op;
    ExpNode *operand;
    UnaryExpNode(char op, ExpNode *operand) : op(op), operand(operand) 
    { 
        type = UNARYEXPNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
    int calconst()
    {
        if (op != '+')
            return (-1) * operand->calconst();
        else
            return operand->calconst();
    }
};

class ExplistNode : public Node {
public:
    std::vector<ExpNode*> exps;
    ExplistNode()
    { 
        type = EXPLISTNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    void append(ExpNode *exp);
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////

class LvalNode : public ExpNode{
public:
    virtual std::string* getName() = 0;
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual int calconst() = 0;
    virtual ~LvalNode(){}
};

class IdNode : public LvalNode{
public:
    std::string *name;
    IdNode(std::string* name) : name(name)
    {
        type = IDNODE;
    }
    int dumpdot(DumpDOT *dumper);
    std::string* getName() { return name; }
    Value *Codegen();
    int calconst()
    {
        return consttable[*name];
    }
};

class IdexpNode : public LvalNode {
public:
    std::string *name;
    ExpNode *expr;
    IdexpNode(std::string *name, ExpNode *expr) : name(name), expr(expr)
    {
        type = IDEXPNODE;
    }
    int dumpdot(DumpDOT *dumper);
    //fix me: what's the name?
    std::string* getName() { return name; }
    Value *Codegen();
    int calconst()
    {
        return 1;
    }
};

//////////////////////////////////////////////////////////////////

class StmtNode : public Node{
public:
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual ~StmtNode(){}
};

class WhileNode : public StmtNode {
public:
    BinaryExpNode *cond;
    StmtNode *stmt;
    WhileNode(BinaryExpNode *cond, StmtNode *stmt) : cond(cond), stmt(stmt)
    {
        type = WHILENODE;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class BreakNode : public StmtNode {
public:
    BreakNode(){}
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class ContinueNode : public StmtNode {
public:
    ContinueNode(){}
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class IfNode : public StmtNode {
public:
    BinaryExpNode *cond;
    StmtNode *stmt1;
    StmtNode *stmt2;
    IfNode(BinaryExpNode *cond, StmtNode *stmt1, 
        StmtNode *stmt2) : cond(cond), stmt1(stmt1), stmt2(stmt2)
    {
        type = IFNODE;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class AsgnNode : public StmtNode {
public:
    LvalNode *lval;
    ExpNode *expr;
    AsgnNode(LvalNode *lval, ExpNode *expr) : lval(lval), expr(expr)
    {
        type = ASGNNODE;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class FunCallNode : public StmtNode {
public:
    std::string *name;
    FunCallNode(std::string *name) : name(name)
    {
        type = FUNCALLNODE;
    }
    int dumpdot(DumpDOT *dumper);
    std::string* getName() { return name; }
    Value *Codegen();
};

////////////////////////////////////////////////////

class BlockilNode : public Node {
public:
    std::vector<Node*> bis;
    BlockilNode()
    { 
        type = BLOCKILNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    void append(Node *bi);
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////

class DeclNode : public Node{
public:
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual ~DeclNode(){}
};

class VarNode : public Node{
public:
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual ~VarNode(){}
};

class Var1Node : public VarNode{
public:
    std::string *name;
    ExpNode *expr;
    Var1Node(std::string *name, ExpNode *expr) : name(name), expr(expr)
    {
        type = VAR1NODE;
    }; 
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class VarArrNode : public VarNode{
public:
    std::string *name;
    ExpNode *expr;
    ExplistNode *explist;
    VarArrNode(std::string *name, ExpNode *expr, 
        ExplistNode *explist) : name(name), expr(expr), explist(explist)
    {
        type = VARARRNODE;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class VarlistNode : public DeclNode {
public:
    std::vector<VarNode*> vars;
    VarlistNode()
    {
        type = VARLISTNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    void append(VarNode* var);
    Value *Codegen();
};

class ConstNode : public Node{
public:
    virtual int dumpdot(DumpDOT *dumper) = 0;
    virtual Value *Codegen() = 0;
    virtual ~ConstNode(){}
};

class Const1Node : public ConstNode{
public:
    std::string *name;
    ExpNode *expr;
    Const1Node(std::string *name, ExpNode *expr) : name(name), expr(expr)
    {
        type = CONST1NODE;
    };
    void mutconst(int valll)
    {
        consttable[*name] = valll;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class ConstArrNode : public ConstNode{
public:
    std::string *name;
    ExpNode *expr;
    ExplistNode *explist;
    ConstArrNode(std::string *name, ExpNode *expr, 
        ExplistNode *explist) :name(name), expr(expr), explist(explist)
    {
        type = CONSTARRNODE;
    };
    void mutconst(int valll)
    {
        ;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

class ConstlistNode : public DeclNode {
public:
    std::vector<ConstNode*> cds;
    ConstlistNode()
    {
        type = CONSTLISTNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    void append(ConstNode* cd);
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////

class FuncdefNode : public Node{
public:
    std::string *name;
    BlockilNode *block;
    FuncdefNode(std::string *name, BlockilNode *block) : name(name), block(block)
    {
        type = FUNCDEFNODE;
    }
    int dumpdot(DumpDOT *dumper);
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////

class SssNode : public Node {
public:
    std::vector<Node*> tops;
    SssNode() 
    {
        type = SSSNODE; 
    };
    int dumpdot(DumpDOT *dumper);
    void append(Node* top);
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////
#endif

//node

//^exprnode : node

//binnode : exprnode
//unanode : exprnode
//^lvalnode : exprnode
//numnode : exprnode

//idnode : lvalnode
//idexpnode : lvalnode

//^stmtnode : node

//asgnnode : stmtnode
//funcallnode : stmtnode
//ifnode : stmtnode
//whilenode : stmtnode

//blockilnode : node

//^declnode : node
//VarlistNode : DeclNode
//constlistnode : declnode

//^varnode : node

//var1 : varnode
//vararr : varnode

//^constnode : node

//const1 : constnode
//constarr : constnode

//funcdefnode : node

