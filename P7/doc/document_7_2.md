#杨智
#PB13011079

#7.2
#遇到的困难及设计思路：

###1. 全局变量和局部变量的区别：
		设置一个标志位isglobal，当进入函数时isglobal＝0，退出时isglobal＝1。当声明的是全局变量时，new GlobalVariable；若声明的是局部变量，则Builder.CreateAlloca。
		
###2. 数组
		用Builder.CreateInBoundGEP即可，要注意deptr。
###3. 作用域
		在刚进入Block::Codegen()时，把符号表保存在这个Codegen方法的局部变量里面；退出Block::Codegen()时，用这个局部变量来恢复符号表。
###4. 链接静态库
		在IR_DEMO里就有例子了，照做就行。我只完成了scan()和print()函数，用过全局变量Output和Input来访问。

#尚未完成的工作
###1. const int变量不可更改的性质
###2. 给const int变量赋初值
		我采用的方法是在ast上对表达式进行计算。但尚不支持表达式里含有变量的情况，如const int i = j + 1;
		
#扩展部分
###实现了break和continue语句