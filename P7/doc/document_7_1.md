#杨智
#PB13011079

#7.1 理解Kaleidoscope代码生成

###阅读LLVM教程的第三章、第五章和第七章，学习LLVM生成IR的方法并回答下列问题（你可以使用compub/llvm3.6.0中的网页镜像和复制好的代码）：

1. 解释教程3.2节中Module、IRBuilder<>的作用；结合教程7.7节，说明: Kaleidoscope的符号表使用了什么结构、如何处理不同作用域的变量重名问题?

		 Module是一个LLVM中的结构，它保存了所有的函数和全局变量。它是LLVM IR用来保存代码的最顶层的结构体。
		 Builder对象是用来产生LLVM指令的。IRBuilder<>可以一方面跟踪插入指令的位置，另一方面可以用来产生新的指令。
		 由std::vector<std::pair<std::string, ExprAST*> > VarNames可见，Kaleidoscope的符号表用了vector来保存所有的变量，每一个变量用<变量名, 该变量在AST上的位置>这样的二元对来表示。
		 在一个作用域里面，每次我们把一个变量放入符号表之前，我们先把被替换的重名的变量的旧值保存在std::vector<AllocaInst *> OldBindings里面。退出该作用域的时候，我们再根据OldBindings来恢复旧变量。
		 
2. 为何使用常量时用的函数名都是get而不是create？

 		tutorial中原话是这样的：
 		Note that in the LLVM IR that constants are all uniqued together and shared. For this reason, the API uses the “foo::get(...)” idiom instead of “new foo(..)” or “foo::Create(..)”.
 		在LLVM IR里面，所有的常量都是唯一、仅有的，并且是共享的，所以我们取常量时用的是get，而不是create。若每次都new，则相同的常量也无法共享，无法进行后续的优化。


3. 简要说明声明和定义一个函数的过程（你可以仿照它使C1支持含参数的函数）。
		
		对函数的声明和定义都是同样的处理：碰到一个function的时候，先对prototype进行codegen。首先根据给出的函数原型的返回值类型和参数的类型与个数，得出FunctionType。之后，尝试把函数名注册在符号表中。若注册不成功，就说明发生了函数名的重名。当函数名重名的时候，设这两个函数分别为原函数和新参数，把原函数从themodule中去掉，再把新函数加入到themodule里面。如果原函数的body部分不为空，就说明这个函数在之前就已经定义过了（如果原函数的body为空，就说明这个函数只是被声明了而已），于是报错；如果原函数的参数数量和新函数的参数数量不一致，就报错。执行完prototype::codegen之后，就对函数的body部分生成LLVM中间代码，这部分和代码的其它地方差别不大。
		
		
4. 处理函数定义时只有一个Block，但是我们可以参考If/Then/Else部分，学习如何操作多个Block。此外，请解释为何需要ThenBB = Builder.GetInsertBlock()。

		第一句不是问题吧？...
		我们需要ThenBB = Builder.GetInsertBlock()的原因是"Then" expression本身可能会改变Builder插入代码的block，也就是说，在对"Then"进行codegen之后，有可能InsertBlock就不再是原来的Block了，所以要对ThenBB的值进行更新。例如，如果代码中出现循环的嵌套，那么就可能发生这种情况。所以，在递归地进行codegen的时候，就需要ThenBB = Builder.GetInsertBlock()来保持Phi Node结果的正确性。
		
5. for循环的Codegen有一处不符合C规定，它先执行一遍循环体，再检查循环条件。思考如何修改可以先检查循环条件，如果为真再执行循环体（用在C1中while的Codegen）
	
		因为是先Body->Codegen()，之后才End->Codegen()，所以原代码产生的for循环会先执行一次循环体，再检查循环条件。要使它符合C规定，我们可以把End-Codegen()及其相关代码 与 Body->Codegen()及其相关代码交换位置。
		因为document里面不许贴代码，所以就只是给出了大致的方案。



6. 阅读第7章，学习如何使用alloca在栈上为变量分配内存。7.7节的Local Variables很像C1的局部变量，说明在有多层嵌套时它是如何操作符号表的。
		
		在分配的时候，通过类似这样的方式：%X = alloca i32 就可以为变量分配内存空间。以后要用的时候通过类似这样的方式：%tmp = load i32* %X 就可以使用已分配了空间的变量。注意前者无星号，因为只是分配空间；后者有星号，因为是通过地址来访问的。 
		通过std::vector<AllocaInst *> OldBindings来在多层嵌套时操作符号表。每一个作用域都有自己的OldBindings。在一个作用域里面，每次有一个变量重名的时候，就在符号表中用新变量替换旧变量，并把旧变量的值保存到OldBindings里面。当退出一个作用域就把这个作用域的OldBindings里的变量和值都恢复到符号表中。
		
		