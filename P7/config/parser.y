%{

#include <stdio.h>
#include <string>
#define DEBUG
#define NodeNum 2048
#include "util.h"
#include "node.h"

using namespace llvm;

int stopast;
Node* nodes[NodeNum];
int ni;	//nodes index

extern int yylex ();
extern void yyerror (const char *msg);

//记录location
extern char currentline[200];
typedef struct YYLTYPE {
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1
int placenum = 0;
int placetype[64];
YYLTYPE place[64];
char inbuf[1024][256];
void specifyerror();
void cleanast();

SssNode *root;

%}

%union
{
	int num;
	std::string *name;
	Node *node;
}

%locations

//%precedence error
//%precedence NUMBER IDENTIFIER
%token error NUMBER IDENTIFIER
%right '='
%left EQ NE
%left '<' '>' LE GE
%left DISMATCH
%left '-' '+'
%left '*' '/' '%'
%left UNARY
%left '(' ')'

//%precedence IF
//%precedence ELSE
%token IF ELSE

%token INT VOID
%token BREAK CONTINUE
%token CONST
%token WHILE
%token ODD

%type <num> NUMBER
%type <name> IDENTIFIER
%type <node> decl funcdef constdecl constdeflist
%type <node> constdef vardecl varlist var 
%type <node> explist block blockil blockitem
%type <node> stmt lval cond exp

%start sss

%%

sss 		: /*empty*/
			  {
			  	debug ("sss ::= empty\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		root = new SssNode ();
			  		((Node*)root)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)root;
			  	}
			  }
			| sss decl
			  {
			  	debug("sss ::= sss decl\n");
			  	if (stopast)
			  		;
			  	else
			  		root->append ((Node*)$2);
			  }
			| sss funcdef
			  {
			  	debug("sss ::= sss funcdef\n");
			  	if (stopast)
			  		;
			  	else			  
			  		root->append ((Node*)$2);
			  }
			;

decl		: constdecl
			  {
				debug("decl ::= constdecl\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $1;
			  }
			| vardecl
			  {
				debug("decl ::= vardecl\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $1;
			  }
			;

constdecl	: CONST INT constdeflist ';'
			  {
				debug("constdecl ::= CONST INT constdeflist ;\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $3;
			  }
			| CONST constdeflist ';'
			  {
			    debug("constdecl ::= CONST error constdeflist ;\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $2;
				place[placenum] = @2;
				placetype[placenum++] = 1;
			  }
			;

constdeflist: constdef
			  {
				debug("constdeflist ::= constdef\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new ConstlistNode();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
					((ConstlistNode*)$$)->append((ConstNode*)$1);	
				}		  
			  }
			| constdeflist ',' constdef
			  {
				debug("constdeflist ::= constdeflist , constdef\n");
			  	if (stopast)
			  		;
			  	else
					((ConstlistNode*)$$)->append((ConstNode*)$3);
			  }
			;

constdef 	: IDENTIFIER '=' exp
			  {
				debug("constdef ::= IDENTIFIER = exp\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new Const1Node($1, (ExpNode*)$3);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '[' exp ']' '=' '{' explist '}' 
			  {
				debug("constdef ::= IDENTIFIER [ exp ] = { explist }\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new ConstArrNode($1, (ExpNode*)$3, (ExplistNode*)$7);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '[' ']' '=' '{' explist '}' 
			  {
				debug("constdef ::= IDENTIFIER [ ] = { explist }\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new ConstArrNode($1, NULL, (ExplistNode*)$6);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			;

vardecl		: INT varlist ';'
			  {
				debug("vardecl ::= INT varlist ;\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $2;
			  }
			;

varlist		: var 
			  {
				debug("varlist ::= var\n");
			  	if (stopast)
			  		;
			  	else
				{
					$$ = new VarlistNode();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
					((VarlistNode*)$$)->append((VarNode*)$1);
				}
			  }
			| varlist ',' var
			  {
				debug("varlist ::= varlist , var\n");
			  	if (stopast)
			  		;
			  	else
					((VarlistNode*)$$)->append((VarNode*)$3);
			  }
			;

//声明
var			: IDENTIFIER
			  {
				debug("var ::= IDENTIFIER\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new Var1Node($1, NULL);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '[' exp ']'
			  {
				debug("var ::= IDENTIFIER [ exp ]\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new VarArrNode($1, (ExpNode*)$3, NULL);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '=' exp
			  {
				debug("var ::= IDENTIFIER = exp\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new Var1Node($1, (ExpNode*)$3);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '[' exp ']' '=' '{' explist '}'
			  {
				debug("var ::= IDENTIFIER [ exp ] = { explist }\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new VarArrNode($1, (ExpNode*)$3, (ExplistNode*)$7);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			; 

explist		: exp
			  {
				debug("explist ::= exp\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new ExplistNode();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
					((ExplistNode*)$$)->append((ExpNode*)$1);
				}
			  }
			| explist ',' exp
			  {
				debug("explist ::= explist , exp\n");
			  	if (stopast)
			  		;
			  	else
					((ExplistNode*)$$)->append((ExpNode*)$3);
			  }
			;

funcdef		: VOID IDENTIFIER '(' ')' block
			  {
				debug("funcdef ::= VOID IDENTIFIER () block\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new FuncdefNode($2, (BlockilNode*)$5);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| VOID IDENTIFIER ')' block
			  {
			  	debug("funcdef ::= VOID IDENTIFIER ) block\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @3;
				placetype[placenum++] = 2;
			  }
			| VOID IDENTIFIER '(' block
			  {
			  	debug("funcdef ::= VOID IDENTIFIER ( block\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @3;
				placetype[placenum++] = 2;
			  }
			;

block 		: '{' blockil '}'
			  {
				debug("block ::= { blockil }\n");
			  	if (stopast)
			  		;
			  	else
			  		$$ = (BlockilNode*)$2;
			  }
			;

blockil		: /*empty*/
			  {
				debug("blockil ::= blockitem\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new BlockilNode ();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| blockil blockitem
			  {
				debug("blockil ::= blockil blockitem\n");
			  	if (stopast)
			  		;
			  	else
					((BlockilNode*)$$)->append((Node*)$2);
			  }
			;

blockitem	: decl
			  {
				debug("blockitem ::= decl\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $1;
			  }
			| stmt
			  {
				debug("blockitem ::= stmt\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $1;
			  }			
			;

stmt 		: lval '=' exp ';'
			  {
				debug("stmt ::= lval = exp ;\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new AsgnNode ((LvalNode*)$1, (ExpNode*)$3);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '(' ')' ';'
			  {
				debug("stmt ::= IDENTIFIER ( ) ;\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new FunCallNode($1);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER ')' ';'
			  {
			  	debug("stmt ::= IDENTIFIER ) ;\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @2;
				placetype[placenum++] = 2;
			  }
			| IDENTIFIER '(' ';'
			  {
			  	debug("stmt ::= IDENTIFIER ( ;\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @2;
				placetype[placenum++] = 2;
			  }
			| block
			  {
				debug("stmt ::= block\n");
			  	if (stopast)
			  		;
			  	else
					$$ = $1;
			  }
			| IF '(' cond ')' stmt ELSE stmt %prec ELSE
			  {
				debug("stmt ::= IF ( cond ) stmt ELSE stmt\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new IfNode((BinaryExpNode*)$3, (StmtNode*)$5, (StmtNode*)$7);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IF '(' cond stmt ELSE stmt %prec ELSE
			  {
			  	debug("stmt ::= IF ( cond stmt ELSE stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| IF cond ')' stmt ELSE stmt %prec ELSE
			  {
			  	debug("stmt ::= IF cond ) stmt ELSE stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| IF '(' cond ')' stmt %prec IF
			  {
				debug("stmt ::= IF ( cond ) stmt \n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new IfNode((BinaryExpNode*)$3, (StmtNode*)$5, NULL);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IF '(' cond stmt %prec IF
			  {
			  	debug("stmt ::= IF ( cond stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| IF cond ')' stmt %prec IF
			  {
			  	debug("stmt ::= IF cond ) stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| WHILE '(' cond ')' stmt
			  {
				debug("stmt ::= WHILE ( cond ) stmt\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new WhileNode((BinaryExpNode*)$3, (StmtNode*)$5);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| WHILE '(' cond stmt
			  {
			  	debug("stmt ::= WHILE ( cond stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| WHILE cond ')' stmt
			  {
			  	debug("stmt ::= WHILE cond ) stmt\n");
				if (stopast)
					;
				else 
				{
			  		stopast = 1;
					cleanast();
				}	
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| ';'
			  {
				debug("stmt ::= ;\n");
			  }
			| BREAK ';'
			  {
			  	debug("stmt ::= break\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new BreakNode();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| CONTINUE ';'
			  {
			  	debug("stmt ::= continue\n");
			  	if (stopast)
			  		;
			  	else
			  	{
			  		$$ = new ContinueNode();
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			;

lval		: IDENTIFIER
			  {
				debug("lval ::= IDENTIFIER\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new IdNode($1);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			| IDENTIFIER '[' exp ']'
			  {
				debug("lval ::= IDENTIFIER [ exp ]\n");
			  	if (stopast)
			  		;
			  	else
			  	{
					$$ = new IdexpNode ($1, (ExpNode*)$3);
			  		((Node*)$$)->setLoc((Loc*)&(@$));
			  		nodes[ni++] = (Node*)$$;
			  	}
			  }
			;



cond
	: exp '<' exp %prec '<'
	{
		debug("cond ::= exp < exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"<", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	| exp '>' exp %prec '>'
	{
		debug("cond ::= exp > exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)">", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	| exp LE exp %prec LE
	{
		debug("cond ::= exp LE exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"<=", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	| exp GE exp %prec GE
	{
		debug("cond ::= exp GE exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)">=", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	| exp EQ exp %prec EQ
	{
		debug("cond ::= exp EQ exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"==", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	| exp NE exp %prec NE
	{
		debug("cond ::= exp NE exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"!=", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	}
	;


exp
	: exp '+' exp %prec '+'
	  {
	  	debug("exp ::= exp + exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"+", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| exp '-' exp %prec '-'
	  {
	  	debug("exp ::= exp - exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"-", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| exp '*' exp %prec '*'
	  {
	  	debug("exp ::= exp * exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"*", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| exp '/' exp %prec '/'
	  {
	  	debug("exp ::= exp / exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"/", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| exp '%' exp %prec '%'
	  {
	  	debug("exp ::= exp % exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new BinaryExpNode ((char*)"%", (ExpNode*)$1, (ExpNode*)$3);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| '(' exp ')' %prec '('
	  {
	  	debug("exp ::= ( exp )\n");
		if (stopast)
			;
		else
	  		$$ = $2;
	  }
	| '-' exp %prec UNARY
	  {
	  	debug("exp ::= - exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new UnaryExpNode('-', (ExpNode*)$2);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| '+' exp %prec UNARY
	  {
	  	debug("exp ::= + exp\n");
		if (stopast)
			;
		else
		{
	  		$$ = new UnaryExpNode('+', (ExpNode*)$2);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| NUMBER
	  {
	  	debug("exp ::= NUMBER\n");
		if (stopast)
			;
		else
		{
	  		$$ = new NumNode($1);
			((Node*)$$)->setLoc((Loc*)&(@$));
			nodes[ni++] = (Node*)$$;
		}
	  }
	| lval
	  {
	  	debug("exp ::= lval\n");
		if (stopast)
			;
		else
	  		$$ = $1;
	  }
	| '(' exp %prec '='
	  {
	  	debug("exp ::= ( exp\n");
		if (stopast)
			;
		else 
		{
			stopast = 1;
			cleanast();
		}	
		place[placenum] = @1;
		placetype[placenum++] = 2;
	  }
	| exp error ')' %prec '='
	  {
	  	debug("exp ::= exp )\n");
		if (stopast)
			;
		else 
		{
			stopast = 1;
			cleanast();
		}	
		place[placenum] = @2;
		placetype[placenum++] = 2;
	  }
	| exp error exp %prec '-'
	  {
	    debug("exp ::= exp exp\n");
		if (stopast)
			;
		else 
		{
			stopast = 1;
			cleanast();
		}	
	    place[placenum] = @2;
	    placetype[placenum++] = 3;
	  }
	;



%%

void yyerror (const char *msg)
{
    printf("%s\n", msg);
}

void specifyerror()
{
	int i;
	for (i = 0; i < placenum; i++)
	{
		switch (placetype[i])
		{
			case 1: 
				printf("<warning> expecting type here ");
				break;
			case 2: 
				printf("<error> mismatched '(' or ')' ");
				break;
			case 3: 
				printf("<error> expecting op here ");
				break;
			case 4: 
				printf("<error> expecting '(' or ')' here ");
				break;
			default:;
		}
		printf("in %d.%d-%d.%d\n%s\n", place[i].first_line, place[i].first_column,
			place[i].last_line, place[i].last_column, inbuf[place[i].first_line - 1]);
		for (int j = 1 ; j < place[i].first_column ; j++)
		{
			putchar('.');
		}
		printf("^\n");
	}
	placenum = 0;
}

void cleanast()
{
	for (ni--; ni >= 0; ni--)
	{
		printf("delete node %d in %d.%d-%d.%d\n", 
		nodes[ni]->type,
		nodes[ni]->loc->first_line, 
		nodes[ni]->loc->first_column,
		nodes[ni]->loc->last_line, 
		nodes[ni]->loc->last_column);
		delete nodes[ni];
	}
}

void printnodes()
{
	for (ni--; ni >= 0; ni--)
	{
		printf("we have node %d in %d.%d-%d.%d\n", 
		nodes[ni]->type,
		nodes[ni]->loc->first_line, 
		nodes[ni]->loc->first_column,
		nodes[ni]->loc->last_line, 
		nodes[ni]->loc->last_column);
		delete nodes[ni];
	}
}
