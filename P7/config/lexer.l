%{

    #include <stdio.h>
    #include <string>
    #include "llvm/IR/Value.h"
    #include "node.h"
    #include "parser.tab.h"

    using namespace llvm;

    static void comment(void);

    extern char inbuf[1024][256];
    extern void specifyerror();
    static int curr_line = 1;
    static int curr_col  = 1;
    static void update_loc(){
        yylloc.first_line   = curr_line;
        yylloc.first_column = curr_col;
        if (*yytext == '\t')
            strcat(inbuf[curr_line - 1], "    ");
        else if(*yytext == '\n')
	;
        else
            strcat(inbuf[curr_line - 1], yytext);
        {
            char * s; 
            for(s = yytext; *s != '\0'; s++)
            {
                if(*s == '\n')
                {
                    specifyerror();
                    curr_line++;
                    curr_col = 1;
                }
                else if(*s == '\t')
                    curr_col = curr_col + 4;
                else
                    curr_col++;
            }
        }
        yylloc.last_line   = curr_line;
        yylloc.last_column = curr_col-1;
    }
    
    #define YY_USER_ACTION update_loc();

%}

O                   [0-7]
D                   [0-9]
NZ                  [1-9]
L                   [a-zA-Z_]
A                   [a-zA-Z_0-9]
H                   [a-fA-F0-9]
HP                  (0[xX])
E                   ([Ee][+-]?{D}+)
P                   ([Pp][+-]?{D}+)
FS                  (f|F|l|L)
IS                  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP                  (u|U|L)
SP                  (u8|u|U|L)
ES                  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS                  [ \t\n\v\f]

%%

"/*"	                { 
                        comment(); 
                        }
"//".*	                { 
                        } 

"const"                 {
                            return CONST; 
                        }
"else"		            { 
                            return ELSE; 
                        }
"if"                    { 
                            return IF; 
                        }
"int"                   { 
                            return INT; 
                        }
"break"                 {
                            return BREAK;
                        }
"continue"              {
                            return CONTINUE;
                        }
"void"		            { 
                            return VOID; 
                        }
"while"                 { 
                            return WHILE; 
                        }
"odd"                   { 
                            return ODD; 
                        }

{L}{A}*		                    { 
                                    yylval.name = new std::string (yytext);
                                    return IDENTIFIER; 
                                }
{HP}{H}+{IS}?					{ 
                                    sscanf(yytext,"%x",&(yylval.num));
                                    return NUMBER; 
                                }
{NZ}{D}*{IS}?					{ 
                                    yylval.num = atoi (yytext);
                                    return NUMBER; 
                                }
"0"{O}*{IS}?					{ 
                                    sscanf(yytext,"%o",&(yylval.num));
                                    return NUMBER; 
                                }

"<="	{ return LE; }
">="	{ return GE; }
"=="	{ return EQ; }
"!="	{ return NE; }
">"     { return '>'; }
"<"     { return '<'; }
";"		{ return ';'; }
"{"		{ return '{'; }
"}"		{ return '}'; }
","		{ return ','; }
"="		{ return '='; }
"("		{ return '('; }
")"		{ return ')'; }
"["		{ return '['; }
"]"		{ return ']'; }
"+"		{ return '+'; }
"-"		{ return '-'; }
"*"		{ return '*'; }
"/"		{ return '/'; }
"%"		{ return '%'; }

{WS}	{ }





%%

int yywrap(void)
{
	return 1;
}

static void comment(void)
{
	int c;
	here:
	while ((c = yyinput()) != 0)
	{
		if (c == '\n')
		{
			curr_line++;
			curr_col = 1;
		}
		else if(c == '\t')
		{
			curr_col = curr_col + 4;
			strcat(inbuf[curr_line - 1], "    ");
		}
		else
		{
			inbuf[curr_line - 1][curr_col - 1] = c;
			curr_col++;
		}
		if (c == '*')
		{
			while ((c = yyinput()) == '*')
			{
				inbuf[curr_line - 1][curr_col - 1] = c;
				curr_col++;
			}
			if (c == '\n')
			{
				curr_line++;
				curr_col = 1;
				goto here;
			}
			else
			{
				if (c == '/')
				{
					inbuf[curr_line - 1][curr_col - 1] = c;
					curr_col++;
					return;
				}
				else if(c == '\t')
				{
					curr_col = curr_col + 4;
					strcat(inbuf[curr_line - 1], "    ");
					goto here;
				}
				else
				{
					inbuf[curr_line - 1][curr_col - 1] = c;
					curr_col++;
					goto here;
				}
			}
		}
	}
}
