#ifndef _NODE_HH_
#define _NODE_HH_

#include <stdio.h>
#include <string>
#include <string.h>
#include <vector>
#include <map>

#include "llvm/IR/Value.h"
using namespace llvm;

//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////

class Node {
public:
    Node() {}
    virtual ~Node(){}
    virtual Value *Codegen() = 0;
};

//////////////////////////////////////////////////////////////////


class StmtNode : public Node {
public: 
    virtual ~StmtNode() {}
    virtual Value *Codegen() = 0;
};

class TypeNode : public Node {
public:
    int type;
    void* info;
    TypeNode(int type, void* info) : type(type), info(info)
    {};
    Value *Codegen();
};

class DecllhsNode;

class Decllhs_noptr_Node : public Node {
public:
    std::string* name;
    DecllhsNode* lhs;
    std::vector<Node*> nodes;
    int sel;
    Decllhs_noptr_Node(std::string* name, DecllhsNode* lhs, int sel) : name(name), lhs(lhs), sel(sel)
    {};
    Value *Codegen();
    void append_A(Node* node, int sel);
};

class DecllhsNode : public Node {
public:
    Decllhs_noptr_Node* dnn;
    int pointer;
    TypeNode* type;
    Type* ty;
    DecllhsNode(Decllhs_noptr_Node* dnn, int pointer) : dnn(dnn), pointer(pointer)
    {};
    Value *Codegen();
};

class ListNode : public Node{
public:
    std::vector<Node*> nodes;
    ListNode() {}
    Value *Codegen();
    void append(Node* node);
};

class StartNode : public Node{
public:
    std::vector<Node*> nodes;
    StartNode() {}
    Value *Codegen();
    void append(Node* node);
};

class BlockNode : public StmtNode {
public:
    ListNode *nodes;
    BlockNode(ListNode *nodes) : nodes(nodes)
    {};
    Value* Codegen();
};


class FuncdefNode : public Node{
public:
    TypeNode* type;
    DecllhsNode* decllhs;
    ListNode* list;
    FuncdefNode(TypeNode* type, DecllhsNode* decllhs, 
        ListNode* list) : type(type), decllhs(decllhs), list(list)
    {};
    Value *Codegen();
};

class JmpNode : public StmtNode {
public:
    ListNode* nodes;
    int sel;
    JmpNode(ListNode* nodes, int sel) : nodes(nodes), sel(sel)
    {};
    Value *Codegen();
};


class ExpstmtNode : public StmtNode {
public:
    ListNode* expr;
    ExpstmtNode(ListNode* expr) : expr(expr)
    {};
    Value *Codegen();
};

class WhileNode : public StmtNode {
public:
    ListNode* cond;
    StmtNode* act;
    WhileNode(ListNode* cond, StmtNode* act) : cond(cond), act(act)
    {};
    Value *Codegen();
};

class ForNode : public StmtNode {
public:
    ExpstmtNode* init;
    ExpstmtNode* cond;
    ListNode* nodes;
    StmtNode* act;
    ForNode(ExpstmtNode* init, ExpstmtNode* cond, ListNode* step,
        StmtNode* act) : init(init), cond(cond), nodes(nodes), act(act)
    {};
    Value *Codegen();
};

class IfelseNode : public StmtNode {
public:
    ListNode* cond;
    StmtNode* truestmt;
    StmtNode* falsestmt;
    IfelseNode(ListNode* cond, StmtNode* truestmt, 
        StmtNode* falsestmt) : cond(cond), truestmt(truestmt), falsestmt(falsestmt)
    {};
    Value *Codegen();
};

class DeclrhsNode : public Node {
public:
    Node* node;
    int sel;
    DeclrhsNode(Node* node, int sel) : node(node), sel(sel)
    {};
    Value *Codegen();
};

class ParadeclarationNode : public Node {
public:
    std::string* type_qua;
    TypeNode* type;
    DecllhsNode* decllhs;
    ParadeclarationNode(std::string* type_qua, TypeNode* type,
        DecllhsNode* decllhs) : type_qua(type_qua), type(type), decllhs(decllhs)
    {};
    Value *Codegen();
};


class InitdeclNode : public Node {
public:
    DecllhsNode* lhs;
    DeclrhsNode* rhs;
    InitdeclNode(DecllhsNode* lhs, DeclrhsNode* rhs) : lhs(lhs), rhs(rhs)
    {};
    Value *Codegen();
};

class DeclarationNode : public Node {
public:
    std::string* type_qua;
    TypeNode* type;
    ListNode* nodes;
    DeclarationNode(std::string* type_qua, TypeNode* type,
        ListNode* nodes) : type_qua(type_qua), type(type), nodes(nodes)
    {};
    Value *Codegen();
};

class BinaryNode : public Node {
public:
    Node* lhs;
    Node* rhs;
    int sel;
    BinaryNode(Node* lhs, int sel, Node* rhs) : lhs(lhs), sel(sel), rhs(rhs)
    {};
    Value *Codegen();
};

class UnaryNode : public Node {
public:
    int sel;
    Node* operand;
    UnaryNode(int sel, Node* operand) : sel(sel), operand(operand)
    {};
    Value *Codegen();
};

class IdNode : public Node{
public:
    std::string* name;
    IdNode(std::string* name) : name(name)
    {};
    Value *Codegen();
};

class PostexpNode : public Node{
public:
    Node* prim;
    std::vector<Node*> nodes;
    std::vector<int> sels;
    PostexpNode(Node* prim) : prim(prim)
    {};
    Value *Codegen();
    void append_A(Node* node);
    void append_sel(int i);
};

class FunccallNode : public Node{
public:
    PostexpNode* pen;
    ListNode* nodes;
    FunccallNode(PostexpNode* pen, ListNode* nodes) : pen(pen), nodes(nodes)
    {};
    Value *Codegen();
};

class NumberNode : public Node{
public:
    int val;
    NumberNode(int val) : val(val)
    {};
    Value *Codegen();
};

class StructtypeNode : public Node{
public:
    std::string* name;
    ListNode* decllist;
    StructtypeNode(std::string* name, ListNode* decllist) : name(name), decllist(decllist)
    {};
    Value *Codegen();
};

//////////////////////////////////////////////////////////////////

#endif
