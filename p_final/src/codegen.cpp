#include "node.h"
#include <llvm/IR/Module.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/PassManager.h>
#include <llvm/Transforms/Scalar.h>
#include "llvm/IR/Verifier.h"
#include <llvm/IR/Type.h>

#include <cstdio>
#include <iostream>
#include <string.h>

using namespace llvm;

Module*TheModule;
IRBuilder<> Builder(getGlobalContext());
FunctionPassManager FPM(TheModule);

static int isglobal = 1;

// record the ouside block
BasicBlock *LastCondBB;
BasicBlock *LastAfterBB;

// basic type
Type* inttype;
Type* voidtype;

// build-in function
Function *func_print;
Function *func_scan;

// function return
BasicBlock* funcblock;
int havereturnstmt;

int decllhs_instruct_flag = 0;

// struct fields table
std::map<Type*, std::map<std::string, int>> fieldtab;

void init()
{
	// init module
	TheModule = new Module("demo", getGlobalContext());
	
  	// Promote allocas to registers.
  	FPM.add(createPromoteMemoryToRegisterPass());
  	// Do simple "peephole" optimizations and bit-twiddling optzns.
  	FPM.add(createInstructionCombiningPass());
  	// Reassociate expressions.
  	FPM.add(createReassociatePass());
  	// Eliminate Common SubExpressions.
  	FPM.add(createGVNPass());
  	// Simplify the control flow graph (deleting unreachable blocks, etc).
  	FPM.add(createCFGSimplificationPass());
  	
    FPM.doInitialization();

    //initial basic type
	inttype = Type::getInt32Ty(TheModule->getContext());
	voidtype = Type::getVoidTy(TheModule->getContext());

    //buildin output and input function
    std::vector<Type*> func_argso;
    func_argso.push_back(inttype);
	FunctionType *func_typeo = 
		FunctionType::get(Type::getVoidTy(getGlobalContext()),
			func_argso, true);

    std::vector<Type*> func_argsi;
    func_argsi.push_back(PointerType::get(inttype, 0));
	FunctionType *func_typei = 
		FunctionType::get(Type::getVoidTy(getGlobalContext()),
			func_argsi, true);
	
	func_print = Function::Create(func_typeo,
		GlobalValue::ExternalLinkage, "print", TheModule);
	func_scan = Function::Create(func_typei,
		GlobalValue::ExternalLinkage, "scan", TheModule);
}

void go()
{
	TheModule->dump();
}

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

// symbol table

struct unit {
	Type* type;	
	Value* val;
};

std::map<std::string, struct unit> symtab;


//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

ConstantInt* getnum(int val)
{
	return ConstantInt::get(TheModule->getContext(), APInt(32, val));
}

FunctionType* getfunctype(Type* rettype, std::vector<Type*> argstype)
{
	printf("in func:getfunctype\n");
	return FunctionType::get(rettype, argstype, false);
}

Function* getfunc(FunctionType* functype, std::string* name)
{
	return Function::Create(functype, Function::ExternalLinkage, 
		*name, TheModule); 
}

Type* getpointertype(Type* type, int pointer)
{
	PointerType* temp = (PointerType*)type;
	for (int i = 0; i < pointer; i++)
		temp = PointerType::get(temp, 0);
	return (Type*)temp;
}

AllocaInst* getalloca(Type* ty, Value* val, std::string name)
{
	AllocaInst* alloca = 
		Builder.CreateAlloca(ty, getnum(0), name);
	Builder.CreateStore(val, alloca);
	return alloca;
}

int noload = 0;

Value* getvalue(Value* alloca)
{
	if (!noload)
		return Builder.CreateLoad(alloca, "temp");
	else
	{
		noload = 0;
		return alloca;
	}
}

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

Value* StartNode::Codegen()
{
	printf("in StartNode::Codegen\n");
	init();
	printf("init succefully\n");
	for (Node* node : nodes)
	{
		node->Codegen();
	}
	printf("Codegen succefully\n");
	go();
	return 0;
}

Value* ListNode::Codegen()
{
	printf("in ListNode::Codegen\n");
	Value* val;
	for (Node* node : nodes)
	{
		val = node->Codegen();
	}
	return val;
}

Value* FuncdefNode::Codegen()
{
	printf("in FuncdefNode::Codegen\n");
	isglobal = 0;
	havereturnstmt = 0;

	//get return type
	Type* rettype = (Type*)type->Codegen();
	int pointer = decllhs->pointer;
	rettype = getpointertype(rettype, pointer);

	//get name and type of args
	printf("getting argsname and argstype\n");
	std::vector<Type*> argstype;
	std::vector<std::string*> argsname;
	Node* temp = decllhs->dnn->nodes.front();
	if (temp != NULL)
	{
		std::vector<Node*> paranodes = ((ListNode*)temp)->nodes;
		for (Node* node : paranodes)
		{
			ParadeclarationNode* temp = (ParadeclarationNode*)node;
			Type* type = (Type*)(temp->type->Codegen());
			int pointer = temp->decllhs->pointer;
			type = getpointertype(type, pointer);
			argstype.push_back(type);
			std::string* name = temp->decllhs->dnn->name;
			argsname.push_back(name);
		}
	}
	printf("got argsname and argstype\n");
	
	//get function
	FunctionType* functype = getfunctype(rettype, argstype);
	printf("got function type\n");
	Function* func = getfunc(functype, decllhs->dnn->name);
	if (func != NULL)
	{
		printf("got function\n");
		std::cout<<*(decllhs->dnn->name)<<std::endl;
	}

	//set block
	BasicBlock* BB = BasicBlock::Create(TheModule->getContext(), "entry", func);
	Builder.SetInsertPoint(BB);
	funcblock = BB;

	//save symbol table
	std::map<std::string, struct unit> tmpsymtab;
	tmpsymtab = symtab;

	//create args
    printf("creating function arguments allocation\n");
	Function::arg_iterator args = func->arg_begin();
  	for (int i = 0, size = argsname.size(); i != size; ++i)
  	{
  		Value* val = args++;
  		val->setName(*(argsname[i]));
    	AllocaInst* alloca = getalloca(argstype[i], val, *(argsname[i]));
    	Builder.CreateStore(val, alloca);
		symtab[*(argsname[i])].val = alloca;
    	symtab[*(argsname[i])].type = argstype[i];
    }
    printf("created function arguments allocation\n");
    
    //gen code for each block item
    if (list != NULL)
    	for (Node* node : list->nodes)
    		node->Codegen();

    // return void if the function has no return statement
  	if (!havereturnstmt)
  		ReturnInst::Create(TheModule->getContext(), 0, BB);

	//optimize function
	FPM.run(*func);
	
	//restore symbol table
    symtab = tmpsymtab;

	isglobal = 1;
    printf("leave FuncdefNode::Codegen\n");
	return 0;
}

Value* JmpNode::Codegen()
{
	printf("in JmpNode::Codegen\n");
	if (sel == 'R')
	{
		havereturnstmt = 1;
		printf("in JmpNode::return::Codegen\n");
		if (nodes == NULL)
			ReturnInst::Create(TheModule->getContext(), 0, Builder.GetInsertBlock());
		else
		{
			AllocaInst* alloca;
			for (Node* node : nodes->nodes)
				alloca = (AllocaInst*)(node->Codegen());
			ReturnInst::Create(TheModule->getContext(), getvalue(alloca), Builder.GetInsertBlock());
		}
	}
	else if(sel == 'C')
	{
		Builder.CreateBr(LastCondBB);
	}
	else if(sel == 'B')
	{
		Builder.CreateBr(LastAfterBB);
	}
	return 0;
}

Value* WhileNode::Codegen()
{
	Function *TheFunction = Builder.GetInsertBlock()->getParent();
	BasicBlock *CondBB = BasicBlock::Create(getGlobalContext(), "cond", TheFunction);
	BasicBlock *LoopBB = BasicBlock::Create(getGlobalContext(), "loop", TheFunction);
	BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "after", TheFunction);
	BasicBlock *tmpLastCondBB = LastCondBB;
	BasicBlock *tmpLastAfterBB = LastAfterBB;
	LastCondBB = CondBB;
	LastAfterBB = AfterBB;
	Builder.CreateBr(CondBB);
	Builder.SetInsertPoint(CondBB);
	Value *CondVal = getvalue(cond->Codegen());
	Builder.CreateCondBr(CondVal, LoopBB, AfterBB);
	Builder.SetInsertPoint(LoopBB);
	act->Codegen();
	Builder.CreateBr(CondBB);
	Builder.SetInsertPoint(AfterBB);
	LastCondBB = tmpLastCondBB;
	LastAfterBB = tmpLastAfterBB;
	return 0;
}

Value* ForNode::Codegen()
{
	return 0;
}

Value* IfelseNode::Codegen()
{
	Function *TheFunction = Builder.GetInsertBlock()->getParent();
	BasicBlock *TrueBB = BasicBlock::Create(getGlobalContext(), "true", TheFunction);
	BasicBlock *FalseBB;
	BasicBlock *AfterBB = BasicBlock::Create(getGlobalContext(), "after", TheFunction);
	Value *CondV = getvalue(cond->Codegen());
	if (falsestmt != NULL)
	{
		FalseBB = BasicBlock::Create(getGlobalContext(), "false", TheFunction);
		Builder.CreateCondBr(CondV, TrueBB, FalseBB);
	}
	else 
		Builder.CreateCondBr(CondV, TrueBB, AfterBB);
	Builder.SetInsertPoint(TrueBB);
	truestmt->Codegen();
	Builder.CreateBr(AfterBB);
	if (falsestmt != NULL) {
		Builder.SetInsertPoint(FalseBB);
		falsestmt->Codegen();
		Builder.CreateBr(AfterBB);
	}
	Builder.SetInsertPoint(AfterBB);
	return 0;
}

Value* ExpstmtNode::Codegen()
{
	for (Node* node : expr->nodes)
	{
		node->Codegen();
	}
	noload = 0;
	return 0;
}

Value* BlockNode::Codegen()
{
	//save symbol table
	std::map<std::string, struct unit> tmpsymtab;
	tmpsymtab = symtab;
    for (Node* node : nodes->nodes) 
    {
        node->Codegen();
    }
    //restore symbol table
    symtab = tmpsymtab; 
	noload = 0;  
    return 0;
}

Value* DeclrhsNode::Codegen()
{
	printf("in DeclrhsNode::Codegen\n");
 	if (sel == 'O')
 		return node->Codegen();
 	else if (sel == 'L')
 	{
 		std::vector<Value*> vals;
 		Value* val;
 		for (Node* node : ((ListNode*)node)->nodes)
 		{
 			val = node->Codegen();
 			vals.push_back(val);
 		}
 		Type* arrty = (Type*)ArrayType::get(val->getType(), (((ListNode*)node)->nodes).size());
		Value* arr = Builder.CreateAlloca(arrty);
		return arr;
 	}
 	else
 		return 0;
}

// congregated
Value* ParadeclarationNode::Codegen()
{
	return 0;
}

// congregated
Value* Decllhs_noptr_Node::Codegen()
{
 	return NULL;
}

Value* DecllhsNode::Codegen()
{
	printf("in DecllhsNode::Codegen\n");
	if(ty == NULL)
		ty = (Type*)(type->Codegen());
	ty = getpointertype(ty, pointer);
	for (Node* node : dnn->nodes)
	{
		ty = (Type*)ArrayType::get(ty, ((NumberNode*)((PostexpNode*)node)->prim)->val);
	}
	printf("in DecllhsNode::Codegen 2\n");
	if (dnn->lhs != NULL)
		dnn->lhs->ty = ty;
	if (decllhs_instruct_flag == 0)
	{
		if (dnn->name != NULL)
		{
			AllocaInst* alloca = Builder.CreateAlloca(ty);
			printf("in DecllhsNode::Codegen 3\n");
			symtab[*(dnn->name)].val = alloca;
			symtab[*(dnn->name)].type = ty;
			printf("leave DecllhsNode::Codegen\n");
			return alloca;
		}
		else
		{
			return dnn->lhs->Codegen();
		}
	}
	else
	{
		if (dnn->name != NULL)
		{
			return (Value*)ty;
		}
		else
		{
			return (Value*)(dnn->lhs->Codegen());
		}
	}
}

Value* InitdeclNode::Codegen()
{
	printf("in InitdeclNode::Codegen\n");
	Value* alloca = lhs->Codegen();
	if (rhs != NULL)
	{
		Value* rval = rhs->Codegen();
		Builder.CreateStore(getvalue(rval), alloca);
	}
	printf("leave InitdeclNode::Codegen\n");
	return 0;
}

Value* DeclarationNode::Codegen()
{
	printf("in DeclarationNode::Codegen\n");
	for (Node* node : nodes->nodes)
		node->Codegen();
	return 0;
}

Value* TypeNode::Codegen()
{
	printf("in TypeNode::Codegen\n");
	if (type == 'V')
		return (Value*)voidtype;
	else if (type == 'I')
		return (Value*)inttype;
	else if (type == 'S')
		return ((Node*)info)->Codegen();
	else
		return 0;
}

Value* BinaryNode::Codegen()
{	
	printf("in BinaryNode::Codegen\n");
	Value* l;
	Value* lval; 
	Value* r;
	Value* rval;
	Value* res;
	if (sel != '&' * 2 + 200 && sel != '|' * 2 + 200)
	{
		l = lhs->Codegen();
		r = rhs->Codegen();
	}
	Function* thefunc = Builder.GetInsertBlock()->getParent();
	BasicBlock* keepcal;
	BasicBlock* skipcal;

	switch(sel)
	{
		case '*':
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateMul(lval, rval, "multmp");
			return (Value*)getalloca(res->getType(), res, "mulres");
		case '/':
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateSDiv(lval, rval, "divtmp");
			return (Value*)getalloca(res->getType(), res, "divres");
		case '%':
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateSRem(lval, rval, "remtmp");
			return (Value*)getalloca(res->getType(), res, "remres");
		case '+':
			rval = getvalue(r);
			lval = getvalue(l);
			if (rval->getType()->isPointerTy() && lval->getType()->isIntegerTy())
			{	
				res = Builder.CreateInBoundsGEP(rval, lval, "addptrtmp");
				return (Value*)getalloca(res->getType(), res, "addptr");
			}
			if (lval->getType()->isPointerTy() && rval->getType()->isIntegerTy())
			{	
				res = Builder.CreateInBoundsGEP(lval, rval, "addptrtmp");
				return (Value*)getalloca(res->getType(), res, "addptr");
			}
			res = Builder.CreateAdd(lval, rval, "addtmp");
			return (Value*)getalloca(res->getType(), res, "addres");
		case '-':
			rval = getvalue(r);
			lval = getvalue(l);
			if (lval->getType()->isPointerTy() && rval->getType()->isIntegerTy())
			{
				res = Builder.CreateInBoundsGEP(lval, 
					Builder.CreateSub(getnum(0), rval, "subtmp"), "subptr");
				return (Value*)getalloca(res->getType(), res, "addptr");
			}
			res = Builder.CreateSub(lval, rval, "subtmp");
			return (Value*)getalloca(res->getType(), res, "subres");
		case '<':
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpSLT(lval, rval, "lttmp");
			return (Value*)getalloca(res->getType(), res, "ltres");
		case '>':
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpSGT(lval, rval, "gttmp");
			return (Value*)getalloca(res->getType(), res, "gtres");
		case '<' + '=' + 400:
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpSLE(lval, rval, "letmp");
			return (Value*)getalloca(res->getType(), res, "leres");
		case '>' + '=' + 400:
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpSGE(lval, rval, "getmp");
			return (Value*)getalloca(res->getType(), res, "geres");
		case '=' * 2 + 200:
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpEQ(lval, rval, "eqtmp");
			return (Value*)getalloca(res->getType(), res, "eqres");
		case '!' + '=' + 400:
			rval = getvalue(r);
			lval = getvalue(l);
			res = Builder.CreateICmpNE(lval, rval, "netmp");
			return (Value*)getalloca(res->getType(), res, "neres");
		case '&' * 2 + 200:
			keepcal = BasicBlock::Create(TheModule->getContext(), "keepcal", thefunc);
			skipcal = BasicBlock::Create(TheModule->getContext(), "skipcal", thefunc);
			l = lhs->Codegen();
			lval = getvalue(l);
			Builder.CreateCondBr(lval, keepcal, skipcal);
			Builder.SetInsertPoint(keepcal);
			r = rhs->Codegen();
			rval = getvalue(r);
			res = Builder.CreateAnd(lval, rval, "andtmp");
			l = getalloca(res->getType(), res, "stmp");
			Builder.SetInsertPoint(skipcal);
			return (Value*)l;
		case '|' * 2 + 200:
			keepcal = BasicBlock::Create(TheModule->getContext(), "keepcal", thefunc);
			skipcal = BasicBlock::Create(TheModule->getContext(), "skipcal", thefunc);
			l = lhs->Codegen();
			lval = getvalue(l);
			Builder.CreateCondBr(lval, skipcal, keepcal);
			Builder.SetInsertPoint(keepcal);
			r = rhs->Codegen();
			rval = getvalue(r);
			res = Builder.CreateAnd(lval, rval, "andtmp");
			l = getalloca(res->getType(), res, "stmp");
			Builder.SetInsertPoint(skipcal);
			return (Value*)l;
		case '=':
			rval = getvalue(r);
			lval = getvalue(l);
			printf("assigning\n");
			if (lval->getType()->isPointerTy() && rval->getType()->isArrayTy())
			{
				Value* indexs[] = {getnum(0), getnum(0)};
				rval = Builder.CreateInBoundsGEP(r, indexs, "arr2ptr");
			}
			Builder.CreateStore(rval, l);
			return (Value*)l;
	}
	return 0;
}

Value* UnaryNode::Codegen()
{
	printf("in UnaryNode::Codegen\n");
	Value* val = operand->Codegen();
	switch(sel)
	{
		case '&':
		{
			noload = 1;
			printf("deciding noload\n");
			return val;
		}
		case '*':
		{
			return getvalue(val);
		}
		case '+':
		{
			return val;
		}
		case '-':
		{
			Value* res = Builder.CreateSub(getnum(0), getvalue(val), "minustmp");
			return getalloca(res->getType(), res, "minusres");
		}
		default: 
			printf("error!\n");
			return 0;
	}
	return 0;
}

Value* IdNode::Codegen()
{
	printf("in IdNode::Codegen\n");
	Value* temp = symtab[*name].val;
	return temp;
}

Value* FunccallNode::Codegen()
{
	printf("in FunccallNode::Codegen\n");
	std::vector<Value*> paras;
	std::string* name = ((IdNode*)pen->prim)->name;
	Function* calleefunc = TheModule->getFunction(*name);
	printf("divide\n");
	for (Node* node : nodes->nodes)
	{
		paras.push_back(getvalue(node->Codegen()));
	}
	if (!calleefunc)
	{
		std::cout<<*name<<std::endl;
		printf("error here!\n");
	}
	Value* r = Builder.CreateCall(calleefunc, paras);
	printf("leave FunccallNode::Codegen\n");
	if (r->getType() != voidtype)
	{
		return getalloca(r->getType(), r, "calltmp");
	}
	else
	{
		return 0;
	}
}

Value* NumberNode::Codegen()
{
	printf("in NumberNode::Codegen\n");
	Value* numval = getalloca(inttype, getnum(val), "numtmp");
	return numval;
}

Value* PostexpNode::Codegen()
{
	printf("in PostexpNode::Codegen\n");
	Value* temp = prim->Codegen();
	Value* val;
	if (!nodes.empty())
	{
		printf("creating gep\n");
		int i = 0;
		for (Node* node : nodes)
		{
			if (sels[i] == '[')
			{
				printf("case 1\n");
				val = node->Codegen();
				val = getvalue(val);
				if (((AllocaInst*)temp)->getAllocatedType()->isPointerTy())
				{	
					printf("hey there!");
					temp = Builder.CreateInBoundsGEP(getvalue(temp), val, "geptmp");
				}
				else
				{
					Value* indexs[] = {getnum(0), val};
					temp = Builder.CreateInBoundsGEP(temp, indexs, "geptmp");
				}
			}
			else if (sels[i] == '.')
			{
				printf("case 2\n");
				std::string* fieldname = ((IdNode*)node)->name;
				int fieldindex = fieldtab[((AllocaInst*)temp)->getAllocatedType()][*fieldname];
				Value* indexs[] = {getnum(0), getnum(fieldindex)};
				temp = Builder.CreateInBoundsGEP(temp, indexs, "structfieldtmp");
				printf("got field\n");
			}
			else
				printf("case 3\n");
			i++;
		}
		printf("created gep\n");
	}
	return temp;
}

Value* StructtypeNode::Codegen()
{
	printf("in StructtypeNode::Codegen\n");

	decllhs_instruct_flag++;

	if (decllist == NULL)
		return (Value*)(TheModule->getTypeByName("struct_" + *name));

	StructType* structty = StructType::create(TheModule->getContext(), "struct_" + *name);
	std::vector<Type*> structty_fields;

	std::vector<Node*> nodes = decllist->nodes;
	int i = 0;
	for (Node* node : nodes)
	{
		//get InitdeclNode
		Node* tmp = ((DeclarationNode*)node)->nodes->nodes[0];
		Value* tmpty = ((InitdeclNode*)tmp)->lhs->Codegen();
		structty_fields.push_back((Type*)tmpty);
		fieldtab[structty][*(((InitdeclNode*)tmp)->lhs->dnn->name)] = i++;
	}
	
	decllhs_instruct_flag--;
	
	if (structty->isOpaque()) {
 		structty->setBody(structty_fields, false);
 	}
 	
 	return (Value*)structty;
}


