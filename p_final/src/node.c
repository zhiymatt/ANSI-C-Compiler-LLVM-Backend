#include "node.h"
#include "util.h"

void ListNode::append(Node* node) 
{
    nodes.push_back(node);
}

void StartNode::append(Node* node) 
{
    nodes.push_back(node);
}

void PostexpNode::append_A(Node* node) 
{
    nodes.push_back(node);
}

void PostexpNode::append_sel(int i)
{
	sels.push_back(i);
}

void Decllhs_noptr_Node::append_A(Node* node, int sel) 
{
    nodes.push_back(node);
    this->sel = sel;
}
