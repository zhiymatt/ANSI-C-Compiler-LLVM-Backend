// test pointer+integer and pointer<->array

int main()
{
	int a[10];
	int *p;
	a[2] = 233;
	p = a;
	p = p + 2;
	print(*p);
	print(p[0]);
	return 0;
}