// test complex declaration
// expected output: 3

int main()
{
	int (*try[10])[30];
	int (*i)[3];
	int k[3];
	k[0] = 1;
	k[1] = 2;
	k[2] = 3;
	i = &k;
	print((*i)[2]);
	return 0;
}