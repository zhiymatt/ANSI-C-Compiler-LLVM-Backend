// test function declaration with parameters and return value
// expected output: 11
int func(int i, int j)
{
	return i + j;
}

int main()
{
	int k;
	int i = 5;	
	int j = i;
	i = i + 1;
	j = func(i, j);
	print(j);
	return j;
}