// test pointer and getaddress '&'
// expected output: 1 3 9
void main()
{
	int *k;
	int **j;
	int val = 1;
	print(*&val);
	k = &val;
	*k = 3;
	print(val);
	j = &k;
	**j = 9;
	print(val);
}