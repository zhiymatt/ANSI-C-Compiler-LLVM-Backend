// test multi-dimentional array
// expected output: 11

int main()
{
	int i[10][20][30];
	int j;
	j = 3;
	i[1][2][3] = 11;
	j = i[1][2][3];
	print(j);
	return 0;
}