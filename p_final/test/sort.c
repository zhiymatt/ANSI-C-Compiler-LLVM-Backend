void exchange(int *i, int *j)
{
	int temp;
	temp = *i;
	*i = *j;
	*j = temp;
}

int main()
{
	int i;
	int length;
	int array[10];
	scan(&length);
	i = 0;
	while (i < length)
	{
		scan(&(array[i]));
		i = i + 1;
	}
	int j;
	int val;
	int ind;
	i = 0;
	while (i < length - 1)
	{
		val = array[i];
		ind = i;
		j = i + 1;
		while (j < length)
		{
			if (array[j] < val)
			{
				val = array[j];
				ind = j;
			}
			j = j + 1;
		}
		exchange(&(array[ind]), &(array[i]));
		i = i + 1;
	}
	i = 0;
	while (i < length)
	{
		print(array[i]);
		i = i + 1;
	}
	return 0;
}
