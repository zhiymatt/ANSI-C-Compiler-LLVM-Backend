// test exchange function
// expected output: 233 100

void exchange(int *i, int *j)
{
	int temp;
	temp = *i;
	*i = *j;
	*j = temp;
}

int main()
{
	int i;
	int j;
	i = 110;
	j = 233;
	exchange(&i, &j);
	print(i);
	print(j);
	return 0;
}