#!/bin/sh

# Generate IR
./compiler < $1 2> test.ll

# Generate Obj
llc -filetype=obj test.ll -o test.o

# Link
clang test.o mylib.a -o test

# run
./test
