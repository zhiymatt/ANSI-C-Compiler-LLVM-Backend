%{

#include <stdio.h>
#include <string>
#define DEBUG
#define NodeNum 2048
#include "util.h"
#include "node.h"

//using namespace llvm;

int stopast;
//Node* nodes[NodeNum];
int ni;	//nodes index

extern int yylex ();
extern void yyerror (const char *msg);

//记录location
extern char currentline[200];
typedef struct YYLTYPE {
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1
int placenum = 0;
int placetype[64];
YYLTYPE place[64];
char inbuf[1024][256];
//void specifyerror();
//void cleanast();

StartNode *root;

%}

%union
{
	int num;
	std::string *name;
	Node *node;
}

%locations

%token IDENTIFIER NUMBER
%token PTR_OP LE GE EQ NE
%token ANDAND OROR
%token CONST
%token INT VOID
%token STRUCT 
%token IF ELSE WHILE FOR CONTINUE BREAK RETURN

%type <num> NUMBER unary_op pointer

%type <name> IDENTIFIER type_qualifier CONST
%type <node> prim_exp post_exp arg_list unary_exp mul_exp
%type <node> add_exp rel_exp equ_exp andand_exp oror_exp
%type <node> assign_exp exp const_exp type
%type <node> structtype declaration init_declarator_list
%type <node> init_declarator decllhs decllhs_nopointer
%type <node> para_list para_declaration identifier_list
%type <node> declrhs declrhs_list designation designator_list
%type <node> designator stmt block_stmt blockitem_list blockitem
%type <node> exp_stmt ifelse_stmt loop_stmt jmp_stmt funcdef
%type <node> declaration_list starthere

%start starthere

%%

prim_exp
	: IDENTIFIER
	{
		debug("prim_exp := IDENTIFIER\n");
		$$ = new IdNode($1);
	}
	| NUMBER
	{
		debug("prim_exp := NUMBER\n");
		$$ = new NumberNode($1);
	}
	| '(' exp ')'
	{
		debug("prim_exp := '(' exp ')'\n");
		$$ = $2;
	}
	;

post_exp
	: prim_exp
	{
		debug("post_exp := prim_exp\n");
		$$ = new PostexpNode($1);
	}
	| post_exp '[' exp ']'
	{
		debug("post_exp := post_exp '[' exp ']'\n");
		((PostexpNode*)$$)->append_A($3);
		((PostexpNode*)$$)->append_sel('[');
	}
	| post_exp '(' ')'
	{
		debug("post_exp := post_exp '(' ')'\n");
		$$ = new FunccallNode((PostexpNode*)$1, NULL);
	}
	| post_exp '(' arg_list ')'
	{
		debug("post_exp := post_exp '(' arg_list ')'\n");
		$$ = new FunccallNode((PostexpNode*)$1, (ListNode*)$3);
	}
	| post_exp '.' IDENTIFIER
	{
		debug("post_exp := post_exp '.' IDENTIFIER\n");
		((PostexpNode*)$$)->append_A(new IdNode($3));
		((PostexpNode*)$$)->append_sel('.');

	}
	| post_exp PTR_OP IDENTIFIER
	{
		debug("post_exp := post_exp PTR_OP IDENTIFIER\n");
		//$$ = new StructrefNode($1, $3, 'P');
	}
	;

arg_list
	: assign_exp
	{
		debug("arg_list := assign_exp\n");
		$$ = new ListNode();
		((ListNode*)$$)->append($1);
	}
	| arg_list ',' assign_exp
	{
		debug("arg_list := arg_list ',' assign_exp\n");
		((ListNode*)$$)->append($3);
	}
	;

unary_exp
	: post_exp
	{
		debug("unary_exp := post_exp\n");
		$$ = $1;
	}
	| unary_op unary_exp
	{
		debug("unary_exp := unary_op unary_exp\n");
		$$ = new UnaryNode($1, $2);
	}
	;

unary_op
	: '&'
	{
		debug("unary_op := '&'\n");
		$$ = '&';
	}
	| '*'
	{
		debug("unary_op := '*'\n");
		$$ = '*';
	}
	| '+'
	{
		debug("unary_op := '+'\n");
		$$ = '+';
	}
	| '-'
	{
		debug("unary_op := '-'\n");
		$$ = '-';
	}
	;

mul_exp
	: unary_exp
	{
		debug("mul_exp := unary_exp\n");
		$$ = $1;
	}
	| mul_exp '*' unary_exp
	{
		debug("mul_exp := mul_exp '*' unary_exp\n");
		$$ = new BinaryNode($1, '*', $3);
	}
	| mul_exp '/' unary_exp
	{
		debug("mul_exp := mul_exp '/' unary_exp\n");
		$$ = new BinaryNode($1, '/', $3);
	}
	| mul_exp '%' unary_exp
	{
		debug("mul_exp := mul_exp '%' unary_exp\n");
		$$ = new BinaryNode($1, '%', $3);
	}
	;

add_exp
	: mul_exp
	{
		debug("add_exp := mul_exp\n");
		$$ = $1;
	}
	| add_exp '+' mul_exp
	{
		debug("add_exp := add_exp '+' mul_exp\n");
		$$ = new BinaryNode($1, '+', $3);
	}
	| add_exp '-' mul_exp
	{
		debug("add_exp := add_exp '-' mul_exp\n");
		$$ = new BinaryNode($1, '-', $3);
	}
	;

rel_exp
	: add_exp
	{
		debug("rel_exp := add_exp\n");
		$$ = $1;
	}
	| rel_exp '<' add_exp
	{
		debug("rel_exp := rel_exp '<' add_exp\n");
		$$ = new BinaryNode($1, '<', $3);
	}
	| rel_exp '>' add_exp
	{
		debug("rel_exp := rel_exp '>' add_exp\n");
		$$ = new BinaryNode($1, '>', $3);
	}
	| rel_exp LE add_exp
	{
		debug("rel_exp := rel_exp LE add_exp\n");
		$$ = new BinaryNode($1, '<' + '=' + 400, $3);
	}
	| rel_exp GE add_exp
	{
		debug("rel_exp := rel_exp GE add_exp\n");
		$$ = new BinaryNode($1, '>' + '=' + 400, $3);
	}
	;

equ_exp
	: rel_exp
	{
		debug("equ_exp := rel_exp\n");
		$$ = $1;
	}
	| equ_exp EQ rel_exp
	{
		debug("equ_exp := equ_exp EQ rel_exp\n");
		$$ = new BinaryNode($1, '=' * 2 + 200, $3);
	}
	| equ_exp NE rel_exp
	{
		debug("equ_exp := equ_exp NE rel_exp\n");
		$$ = new BinaryNode($1, '!' + '=' + 400, $3);
	}
	;

andand_exp
	: equ_exp
	{
		debug("andand_exp := equ_exp\n");
		$$ = $1;
	}
	| andand_exp ANDAND equ_exp
	{
		debug("andand_exp := andand_exp ANDAND equ_exp\n");
		$$ = new BinaryNode($1, '&' * 2 + 200, $3);
	}
	;

oror_exp
	: andand_exp
	{
		debug("oror_exp := andand_exp\n");
		$$ = $1;
	}
	| oror_exp OROR andand_exp
	{
		debug("oror_exp := oror_exp OROR andand_exp\n");
		$$ = new BinaryNode($1, '|' * 2 + 200, $3);
	}
	;

assign_exp
	: oror_exp
	{
		debug("assign_exp := oror_exp\n");
		$$ = $1;
	}
	| unary_exp '=' assign_exp
	{
		debug("assign_exp := unary_exp '=' assign_exp\n");
		$$ = new BinaryNode($1, '=', $3);
	}
	;

exp
	: assign_exp
	{
		debug("exp := assign_exp\n");
		$$ = new ListNode();
		((ListNode*)$$)->append($1);
	}
	| exp ',' assign_exp
	{
		debug("exp := exp ',' assign_exp\n");
		((ListNode*)$$)->append($3);
	}
	;

// fix me: it seems that it has something do with designation...
const_exp
	: oror_exp
	{
		debug("const_exp := oror_exp\n");
		$$ = $1;
	}
	;

type
	: VOID
	{
		debug("type := VOID\n");
		$$ = new TypeNode('V', NULL);
	}
	| INT
	{
		debug("type := INT\n");
		$$ = new TypeNode('I', NULL);
	}
	| structtype
	{
		debug("type := structtype\n");
		$$ = new TypeNode('S', $1);
	}
	;

type_qualifier
	: CONST
	{
		debug("type_qualifier := CONST\n");
		$$ = $1;
	}
	;

// fix me: just leave it, for now....
structtype
	: STRUCT '{' declaration_list '}'
	{
		debug("structtype := STRUCT '{' declaration_list '}'\n");
		$$ = new StructtypeNode(NULL, (ListNode*)$3);
	}
	| STRUCT IDENTIFIER '{' declaration_list '}'
	{
		debug("structtype := STRUCT IDENTIFIER '{' declaration_list '}'\n");
		$$ = new StructtypeNode($2, (ListNode*)$4);
	}
	| STRUCT IDENTIFIER
	{
		debug("structtype := STRUCT IDENTIFIER\n");
		$$ = new StructtypeNode($2, NULL);
	}
	;

declaration
	: type init_declarator_list ';'
	{
		debug("declaration := type init_declarator_list ';'\n");
		$$ = new DeclarationNode(NULL, (TypeNode*)$1, (ListNode*)$2);
		for (Node* node : ((ListNode*)$2)->nodes)
		{
			((InitdeclNode*)node)->lhs->type = (TypeNode*)$1;
		}
	}
	| type ';'
	{
		debug("declaration := type");
		$$ = $1;
	}
	| type_qualifier type init_declarator_list ';'
	{
		debug("declaration := type_qualifier type init_declarator_list ';'\n");
		$$ = new DeclarationNode($1, (TypeNode*)$2, (ListNode*)$3);
		for (Node* node : ((ListNode*)$3)->nodes)
		{
			((InitdeclNode*)node)->lhs->type = (TypeNode*)$1;
		}
	}
	;

init_declarator_list
	: init_declarator
	{
		debug("init_declarator_list := init_declarator\n");
		$$ = new ListNode();
		((ListNode*)$$)->append($1);
	}
	| init_declarator_list ',' init_declarator
	{
		debug("init_declarator_list := init_declarator_list ',' init_declarator\n");
		((ListNode*)$$)->append($3);
	}
	;

init_declarator
	: decllhs '=' declrhs
	{
		debug("init_declarator := decllhs '=' declrhs\n");
		$$ = new InitdeclNode((DecllhsNode*)$1, (DeclrhsNode*)$3);
	}
	| decllhs
	{
		debug("init_declarator := decllhs\n");
		$$ = new InitdeclNode((DecllhsNode*)$1, NULL);
	}
	;

decllhs
	: pointer decllhs_nopointer
	{
		debug("decllhs := pointer decllhs_nopointer\n");
		$$ = new DecllhsNode((Decllhs_noptr_Node*)$2, $1);
	}
	| decllhs_nopointer
	{
		debug("decllhs := decllhs_nopointer\n");
		$$ = new DecllhsNode((Decllhs_noptr_Node*)$1, 0);
	}
	;

decllhs_nopointer
	: IDENTIFIER
	{
		debug("decllhs_nopointer := IDENTIFIER\n");
		$$ = new Decllhs_noptr_Node($1, NULL, 0);
	}
	| '(' decllhs ')'
	{
		debug("decllhs_nopointer := '(' decllhs ')'\n");
		$$ = new Decllhs_noptr_Node(NULL, (DecllhsNode*)$2, 0);
	}
	| decllhs_nopointer '[' ']'
	{
		debug("decllhs_nopointer := decllhs_nopointer '[' ']'\n");
		((Decllhs_noptr_Node*)$$)->append_A(NULL, 'F');
	}
	| decllhs_nopointer '[' assign_exp ']'
	{
		debug("decllhs_nopointer := decllhs_nopointer '[' assign_exp ']'\n");
		((Decllhs_noptr_Node*)$$)->append_A($3, 'F');
	}
	| decllhs_nopointer '(' para_list ')'
	{
		debug("decllhs_nopointer := decllhs_nopointer '(' para_list ')'\n");
		((Decllhs_noptr_Node*)$$)->append_A($3, 'Y');
	}
	| decllhs_nopointer '(' ')'
	{
		debug("decllhs_nopointer := decllhs_nopointer '(' ')'\n");
		((Decllhs_noptr_Node*)$$)->append_A(NULL, 'Y');
	}
	;

pointer
	: '*' pointer
	{
		debug("pointer := '*' pointer\n");
		$$ = $2 + 1;
	}
	| '*'
	{
		debug("pointer := '*'\n");
		$$ = 1;
	}
	;

para_list
	: para_declaration
	{
		debug("para_list := para_declaration\n");
		$$ = new ListNode();
		((ListNode*)$$)->append((Node*)$1);
	}
	| para_list ',' para_declaration
	{
		debug("para_list := para_list ',' para_declaration\n");
		((ListNode*)$$)->append((Node*)$3);
	}
	;

para_declaration
	: type decllhs
	{
		debug("para_declaration := type decllhs\n");
		$$ = new ParadeclarationNode(NULL, (TypeNode*)$1, (DecllhsNode*)$2);
	}
	| type_qualifier type decllhs
	{
		debug("para_declaration := type_qualifier type decllhs\n");
		$$ = new ParadeclarationNode($1, (TypeNode*)$2, (DecllhsNode*)$3);
	}
	;

identifier_list
	: IDENTIFIER
	{
		debug("identifier_list := IDENTIFIER\n");
		$$ = new ListNode();
		((ListNode*)$$)->append((Node*)$1);
	}
	| identifier_list ',' IDENTIFIER
	{
		debug("identifier_list := identifier_list ',' IDENTIFIER\n");
		((ListNode*)$$)->append((Node*)$3);
	}
	;

// fix me: i have no clue here...
declrhs
	: '{' declrhs_list '}'
	{
		debug("declrhs := '{' declrhs_list '}'\n");
		$$ = new DeclrhsNode((Node*)$2, 'L'); 
	}
	| '{' declrhs_list ',' '}'
	{
		debug("declrhs := '{' declrhs_list ',' '}'\n");
		$$ = new DeclrhsNode((Node*)$2, 'L'); 
	}
	| assign_exp
	{
		debug("declrhs := assign_exp\n");
		$$ = new DeclrhsNode((Node*)$1, 'O'); 
	}
	;

declrhs_list
	: designation declrhs
	{
		debug("declrhs_list := designation declrhs\n");
	}
	| declrhs
	{
		debug("declrhs_list := declrhs\n");
		$$ = new ListNode();
		((ListNode*)$$)->append((Node*)$1);
	}
	| declrhs_list ',' designation declrhs
	{
		debug("declrhs_list := declrhs_list ',' designation declrhs\n");
	}
	| declrhs_list ',' declrhs
	{
		debug("declrhs_list := declrhs_list ',' declrhs\n");
		((ListNode*)$$)->append((Node*)$3);
	}
	;

// fix me: no need to implement this (designation)?
designation
	: designator_list '='
	{
		debug("designation := designator_list '='\n");
	}
	;

designator_list
	: designator
	{
		debug("designator_list := designator\n");
	}
	| designator_list designator
	{
		debug("designator_list := designator_list designator\n");
	}
	;

designator
	: '[' const_exp ']'
	{
		debug("designator := '[' const_exp ']'\n");
	}
	| '.' IDENTIFIER
	{
		debug("designator := '.' IDENTIFIER\n");
	}
	;

stmt
	: block_stmt
	{
		debug("stmt := '.' IDENTIFIER\n");
		$$ = $1;
	}
	| exp_stmt
	{
		debug("stmt := exp_stmt\n");
		$$ = $1;
	}
	| ifelse_stmt
	{
		debug("stmt := ifelse_stmt\n");
		$$ = $1;
	}
	| loop_stmt
	{
		debug("stmt := loop_stmt\n");
		$$ = $1;
	}
	| jmp_stmt
	{
		debug("stmt := jmp_stmt\n");
		$$ = $1;
	}
		;

block_stmt
	: '{' '}'
	{
		debug("block_stmt := '{' '}'\n");
		$$ = new BlockNode(NULL);
	}
	| '{' blockitem_list '}'
	{
		debug("block_stmt := '{' blockitem_list '}'\n");
		$$ = new BlockNode((ListNode*)$2);
	}
	;

blockitem_list
	: blockitem
	{
		debug("blockitem_list := blockitem\n");
		$$ = new ListNode();
		((ListNode*)$$)->append((Node*)$1);
	}
	| blockitem_list blockitem
	{
		debug("blockitem_list := blockitem_list blockitem\n");
		((ListNode*)$$)->append((Node*)$2);
	}
	;

blockitem
	: declaration
	{
		debug("blockitem := declaration\n");
		$$ = $1;
	}
	| stmt
	{
		debug("blockitem := stmt\n");
		$$ = $1;
	}
	;

exp_stmt
	: ';'
	{
		debug("exp_stmt := ';'\n");
		$$ = NULL;
	}
	| exp ';'
	{
		debug("exp_stmt := exp ';'\n");
		$$ = new ExpstmtNode((ListNode*)$1);
	}
	;

ifelse_stmt
	: IF '(' exp ')' stmt ELSE stmt
	{
		debug("ifelse_stmt := IF '(' exp ')' stmt ELSE stmt\n");
		$$ = new IfelseNode((ListNode*)$3, (StmtNode*)$5, (StmtNode*)$7);
	}
	| IF '(' exp ')' stmt
	{
		debug("ifelse_stmt := IF '(' exp ')' stmt\n");
		$$ = new IfelseNode((ListNode*)$3, (StmtNode*)$5, NULL);
	}
	;

loop_stmt
	: WHILE '(' exp ')' stmt
	{
		debug("loop_stmt := WHILE '(' exp ')' stmt\n");
		$$ = new WhileNode((ListNode*)$3, (StmtNode*)$5);
	}
	| FOR '(' exp_stmt exp_stmt ')' stmt
	{
		debug("loop_stmt := FOR '(' exp_stmt exp_stmt ')' stmt\n");
		$$ = new ForNode((ExpstmtNode*)$3, (ExpstmtNode*)$4, NULL, (StmtNode*)$6);
	}
	| FOR '(' exp_stmt exp_stmt exp ')' stmt
	{
		debug("loop_stmt := FOR '(' exp_stmt exp_stmt exp ')' stmt\n");
		$$ = new ForNode((ExpstmtNode*)$3, (ExpstmtNode*)$4, (ListNode*)$5, (StmtNode*)$7);
	}
	;

jmp_stmt
	: CONTINUE ';'
	{
		debug("jmp_stmt := CONTINUE ';'\n");
		$$ = new JmpNode(NULL, 'C');
	}
	| BREAK ';'
	{
		debug("jmp_stmt := BREAK ';'\n");
		$$ = new JmpNode(NULL, 'B');
	}
	| RETURN ';'
	{
		debug("jmp_stmt := RETURN ';'\n");
		$$ = new JmpNode(NULL, 'R');
	}
	| RETURN exp ';'
	{
		debug("jmp_stmt := RETURN exp ';'\n");
		$$ = new JmpNode((ListNode*)$2, 'R');
	}
	;

funcdef
	: type decllhs '{' blockitem_list '}'
	{
		debug("funcdef := type decllhs '(' declaration_list ')' '{' blockitem_list '}'\n");
		$$ = new FuncdefNode((TypeNode*)$1, (DecllhsNode*)$2, (ListNode*)$4);
	}
	| type decllhs '{' '}'
	{
		debug("funcdef := type decllhs '(' declaration_list ')' '{' blockitem_list '}'\n");
		$$ = new FuncdefNode((TypeNode*)$1, (DecllhsNode*)$2, NULL);
	}
	;

declaration_list
	: declaration
	{
		debug("declaration_list := declaration\n");
		$$ = new ListNode();
		((ListNode*)$$)->append((Node*)$1);
	}
	| declaration_list declaration
	{
		debug("declaration_list := declaration_list declaration\n");
		((ListNode*)$$)->append((Node*)$2);
	}
	;

starthere
	: declaration
	{
		debug("starthere := declaration\n");
		$$ = new StartNode();
		((StartNode*)$$)->append((Node*)$1);
		root = (StartNode*)$$;
	}
	| starthere declaration
	{
		debug("starthere := starthere declaration\n");
		((StartNode*)$$)->append((Node*)$2);
	}
	| funcdef
	{
		debug("starthere := funcdef\n");
		$$ = new StartNode();
		((StartNode*)$$)->append((Node*)$1);
		root = (StartNode*)$$;
	}
	| starthere funcdef
	{
		debug("starthere := starthere funcdef\n");
		((StartNode*)$$)->append((Node*)$2);
	}
	;



%%

void yyerror (const char *msg)
{
    printf("%s\n", msg);
}
