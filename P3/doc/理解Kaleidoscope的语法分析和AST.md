#::::::::::::::::::::::::

#3.1. 理解Kaleidoscope的语法分析和AST
###解释include/ast.h中ExprAST里的virtual的作用，在继承时的原理（解释vtable）。
virtual函数是指一个类中你希望重载的成员函数，当你用一个基类指针或引用指向一个继承类对象的时候，你调用一个virtual函数，实际调用的是继承类的版本。

vtable是指一张函数指针表，vtable中的指针指向一个对象支持的接口成员函数。每个虚函数都在vtable中占了一个表项，保存着一条跳转到它的入口地址的指令。当一个包含虚函数的对象被创建时，它在头部附加一个指针，指向vtable中相应位置。调用虚函数时，先根据vtable找到入口地址再执行，从而动态联编。

在继承时，派生类的vtable继承了它各个基类的vtable，如果派生类override了vtable中某一项对应的新函数，则派生类vtable的该项指向override后的虚函数；没有override的话，就沿用基类中该项的值。在ast.h文件中，BinaryExprAST、VariableExprAST等不同的类都继承自ExprAST，但不同的派生类的Codegen、dumpdot函数的实现方式都不同的。所以在基类中要用virtual函数，在派生类中要对virtual进行override，从而让每个派生类的vtable提供同样的接口的同时让表项对应不同的特定的函数。

###本次实验主要关注src/toy.cpp中的MainLoop及其调用的函数。阅读HandleDefinition和HandleTopLevelExpression，忽略Codegen部分，说明两者对应的AST结构
HandleDefinition对应的AST结构

			 functAST
			  /  \
			 /    \
			/      \
		protoAST  exprAST
        /  \       
       /    \      
	name    args
	
HandleTopLevelExpression对应的AST结构

			 functAST
			  /  \
			 /    \
			/      \
		protoAST  exprAST
        /  \       
       /    \      
	name(空) args(空)
	
可以看出两者对应的AST结构基本上是一样的，只不过HandleDefinition中的protoAST的name和args域包办函数的名字和参数；而HandleTopLevelExpression中的protoAST的name和args域都为空，并且HandleTopLevelExpression会把这个匿名函数放到theExecutionEngine中执行。
  
###Kaleidoscope如何在Lexer和Parser间传递信息？（token、语义值、用什么函数和变量）
Lexer中getNextToken函数从输入流中读入一个token，返回值为enum Token中该token对应的值（integer类型），parser则不断调用getNextToken函数来取得token，由此建立AST。token通过变量CurTok来传递，CurTok即gettok和getNextToken函数的返回值

###Kaleidoscope如何处理算符优先级？（重点解释ParseBinOpRHS）分析a\*b\*c、a\*b+c、a+b*c分别是如何处理的？
首先，GetTokPrecedence可以从BinopPrecedence这个变量里面取得运算符号对应的优先级。<br>
parser会把输入的expression做合适的分割，如a\*b\*c会被分割成[a][\* b][\* c]。<br>
ParseExpression会先parse[a],然后调用ParseBinOpRHS来不断处理[binop,primaryexpr]这样的对。其中ParseBinOpRHS(int ExprPrec, ExprAST *LHS)有两个参数，LHS当前二元运算符的左部，ExprPrec是一个优先度的阈值。若当前运算符的优先度小于ExprPrec，则说明当前运算符的左部可以被当作一个AST的子树，否则就继续读入一个binop和一个primary expression，组成[binop,primaryexpr]对。之后再看下一个符号，如果这个符号的优先度小于等于binop的优先度，则此时可以用BinaryExprAST(BinOp, LHS, RHS)构建AST；否则就通过ParseBinOpRHS(TokPrec + 1, RHS)来构成RHS子树。之后继续这样处理。<br>

	a * b * c的处理方式：
			LHS <- a
			RHS <- b
			binop <- *
			因为两个*号的优先级一样，所以
			LHS <- merge(LHS, RHS, binop), 即LHS <- (a * b)
			RHS <- c
			binop = *
			最后
			LHS <- merge(LHS, RHS, binop), 即LHS <- (a * b) * c
			return LHS
------
		
	a * b + c的处理方式：
			LHS <- a
			RHS <- b
			binop <- *
			因为＋的优先度比*低，所以
			LHS <- merge(LHS, RHS, binop), 即LHS <- (a * b)
			RHS <- c
			binop <- +
			最后
			LHS <- merge(LHS, RHS, binop), 即LHS <- (a * b) ＋ c
			return LHS
------

	a + b * c 的处理方式
			LHS <- a
			RHS <- b
			binop <- +
			{
				(这段代码只改变temp的值）
				LHS <- RHS
				RHS <- c
				binop <- *
				LHS <- merge(LHS, RHS, binop), 即LHS <- (b * c)
				temp <- LHS
			}
			RHS <- temp, 即RHS <- (b * c)
			LHS <- merge(LHS, RHS, binop), 即LHS <- a + (b * c)
			return LHS
			
###解释Error, ErrorP, ErrorF的作用，举例说明它们在语法分析中应用。
Error,ErrorP,ErrorF的作用都是打印错误信息，并返回一个NULL。这三个函数不同之处在于返回的类型不一样。在语法分析时，如果出错就返回Error/ErrorP/ErrorF的NULL值。因为语法分析时有很多的递归调用，所以通过这种返回NULL的方式就可以可以在递归的每一层都得以判断是否出错。
###Kaleidoscope不支持声明变量，给变量赋值，那么变量的作用是什么？
kaleidoscope中变量主要是用来抽象地代表某一个数吧。比如在函数里面，编程者在编写函数时并不能够知道调用时实际传递给这个函数的是什么参数，于是编写函数时就需要变量来指代这个数字。又比如在for循环里面，for i = 1, i < n, 1.0 in blablabla，实际上i这个变量在循环过程中并不作为一个可赋值变量被重新赋值，这里的i给我的感觉更像是一种syntactic sugar。
###为什么不需要{...}或begin...end？
因为Kaleidoscope里面没有语句块。比如forexpr ::= 'for' identifier '=' expr ',' expr (',' expr)? 'in' expression就是如此，ifexpr ::= 'if' expression 'then' expression 'else' expression也是如此。<br>
若要实现类似语句块的功能，可以通过如下方式：
		
		# Define ':' for sequencing: as a low-precedence operator that ignores operands		# and just returns the RHS.		def binary : 1 (x y) y;		def fibi(x)  		var a = 1, b = 1, c in  		(for i = 3, i < x in			c=a+b: a=b:			b = c) :		b;		￼fibi(10);
通过这种方式，可以把多个语句连接成多个语句
###以下问题基于第五章: 

###Kaleidoscope是如何避免if/then/else语句的二义性的？
看if/then/else语句的定义就知道了<br>
ifexpr ::= 'if' expression 'then' expression 'else' expression<br>
在if/then/else语句中，三个basicblock缺一不可；此外，在if和then之间，then和else之间，else之后、下一语句之前，都有且只有一个expression。
通过这种对表达能力的限制，我们就可以避免二义性。
###Kaleidoscope只有一种数据类型double，那么if/then/else的分支条件是如何实现的？（你可能要看BinaryExpr和IfExpr的Codegen部分）
在BinaryExpr的Codegen部分，以'<'运算符为例，首先L = Builder.CreateFCmpULT(L, R, "cmptmp")比较binop的lefthandside和righthandside，返回一个bool值，之后Builder.CreateUIToFP(L,Type::getDoubleTy(getGlobalContext()),"booltmp")就可以把这个这个bool值转换为double值。通过这种方式就可以实现在Kaleidoscope里两个数的比较,并把比较结果用double类型表示。<br>
在IfExpr的Codegen部分中可以看出，在'if' expression 'then' expression 'else' expression语句中，先计算expression的值（这个值是double类型的），之后把这个值和0.0做比较，如果这个值不等于0.0，就跳转到then处；否则就跳转到else处。
