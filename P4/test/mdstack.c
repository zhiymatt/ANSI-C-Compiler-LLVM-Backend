int stack[100];
int top;
int pushvalue;
int popvalue;

void pop()
{
    popvalue = stack[top];
    top = top - 1;
}
void push()
{
    top = top + 1;
    stack[top] = pushvalue;
}

void divi()    /*自定义函数求两数的最大公约数*/
{
    int temp;          /*定义整型变量*/
    int a;
    int b;
    pop();
    a = popvalue;
    pop();
    b = popvalue;
    if ( a < b )
    {
        temp = a;
        a = b;
        b = temp;
    }
    while( b != 0 )
    {
        temp = a % b;
        a = b;
        b = temp;
    }
    pushvalue = a;
    push();
}


void mult()
{
    int temp;
    int a;
    int b;
    pop();
    a = popvalue;
    pop();
    b = popvalue;
    pushvalue = a;
    push();
    pushvalue = b;
    push();
    divi();
    pop();
    temp = popvalue;
    pushvalue = ( a * b / temp );
    push();
}

void main(){
    int divivalue;
    int multvalue;
    pushvalue = 21;
    push();
    pushvalue = 35;
    push();
    divi();
    pop();
    divivalue = popvalue;
    pushvalue = 21;
    push();
    pushvalue = 35;
    push();
    mult();
    pop();
    multvalue = popvalue;
}