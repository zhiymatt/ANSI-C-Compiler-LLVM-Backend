PB13011079<br>
杨智<br>
#回答问题
####1. Bison的输入文法规范文件的格式
<pre><code>
		%{			Prologue		%}
		Bison declarations		%%		Grammar rules		%%		Epilogue
</code></pre>
prologue会被直接照原样复制到输出的parser implement file里面；bison declarations为对functions和variables的定义；grammer rules定义语法规则；epilogue里的代码会直接被输出到输出文件的最后。

####2. Bison如何与Flex协作生成某个语言的分析器
	第一步，在.y文件中extern int yylex ();<br>
	第二步，在bison中用-d参数生成两个文件，分别是.tab.c和.tab.h文件<br>
	第三步，在flex的.l文件中include第一步生成出来的.tab.h文件<br>
	最后，根据依赖关系写出makefile即可
####3. 所生成的分析器的接口形式和实现具有什么样的特征
所生成的parser实际上是一个c的函数yyparse。<br>
函数原型是int yyparse (void)，若成功则返回0，若因为无效输入而不成功则返回1，若因为内存用尽而不成功则返回2。<br>
若要传递额外的参数，就用directive：%parse-param {argument-declaration} . . .<br>
还有其它的一些接口如yyerror等，具体内容可以看bison manual的page 97。<br>
(并没有觉得有什么特征..也可能是我没看懂题目)
####4. 语言的文法对生成的分析器的分析表的状态空间有何影响。
同一语言的不同文法生成的分析器的分析表的状态空间的大小和具体状态不同。<br>
状态空间的大小：LR(1) > LALR(1) > SLR(1) > LR(0)<br>
而不同文法的状态空间里的状态自然也是不同的。<br>
LALR(1)里的状态空间可由LR(1)的状态空间经同心圆合并得来。


####1. 查看bison-examples中Makefile文件里名为expr的编译任务，理解任务中各个命令的作用和所使用的选项的含义。
		
		$(YACC) -b $@ -o $(SRC)/$@.tab.c $(CONF)/$@.y
用bison根据.y文件生成.tab.c文件，-b选项用来设定输出文件的文件名，-d把输出文件分成.c文件和.h文件，－y选项没什么用，只是为了和yacc兼容。

		$(LEX) -o$(SRC)/$@.lex.c $(CONF)/expr.lex
用flex根据.lex文件生成.lex.c文件。－I产生interactive scanner，可以从标准输入流中读入输入？-i忽略大小写。

		$(CC) -o $(BIN)/$@ $(SRC)/$@.lex.c $(SRC)/$@.tab.c -ll -lm
用gcc根据.tab.c和.lex.c文件编译生成分析器。其中-Iinclude选项把/include加入到搜索头文件的路径列表中，-g生成调试信息，－ll -lm链接相关的库文件。
####2. bison-examples中config目录下的expr.y和expr1.y均能在进行语法分析的同时完成表达式的求值――语法制导的翻译，通过expr.y和expr1.y理解Yacc输入文法规范文件的格式，消化语法制导的翻译方案。
yacc输入文法规范文件的格式在第一部分第一题里就讲了。语法指导的翻译方案在课上就讲过了，这里好像也没什么必要复述，毕竟expr.y和expr1.y中的语法制导的翻译方案还是挺好写的。expr.y和expr1.y大体上是一样的，主要的不同之处在于，expr.y通过指定算符的优先级和结合性来避免二义性，而expr1.y则是通过重写文法，用expr/term/fact这几个非终结符来体现不同算符的区别，从而避免二义性。
####3. 阅读并对比bison-examples中src目录下的expr.tab.h、expr.tab.c和expr1.tab.c，总结expr.tab.c和expr1.tab.c的异同。说明expr.y和expr1.y中对表达式语言的不同文法表示对生成的分析器源代码expr.tab.c和expr1.tab.c有何影响。
expr.tab.h和expr1.tab.h是一样的。
expr.tab.c和expr1.tab.c文件的整体框架都是一致的，和文法本身的关系不大，是由bison自动生成的。不同之处主要是这两个文件对应的分析表不同。但我觉得不同之处很少。expr.y通过指定算符的优先级和结合性来避免二义性，而expr1.y则是通过重写文法，用expr/term/fact这几个非终结符来体现不同算符的区别，从而避免二义性。expr1中因为多了两个非终结符，所以状态比expr多两个，另外，本来在expr中同属一个状态的LB(1)项目，因为expr1文法的关系，在expr1属于不同的状态。<br>
由此可见，同一种语言可以有不同的文法实现，不同的文法实现对应于不同的.y文件，而.y文件的不同就导致了生成的分析器源代码.tab.c文件的不同。虽然expr.tab.c和expr1.tab.c文件不相同，但这两份源代码都能够实现同样的功能，只是具体实现不同
####4. 阅读、理解bison-examples中config目录下的asgn.y和asgn1.y、src目录下的asgn.1ex.c, asgn.tab.h, asgn.tab.c, asgn1.tab.c，了解L-asgn分析器的构成方法。
（这道题也不知道答什么好）L-asgn分析器主要由两部分构成，一部分是词法分析，另一部分是语法分析。asgn.lex.c和asgn.tab.h一起构成词法分析器，asgn.y、asgn1.y产生的sourcefile可用来产生语法分析器。词法分析把输入流转换为记号流；语法分析根据记号流，通过语法制导的翻译，在进行语法分析的同时完成表达式的求值，并修改符号表中响应的条目。
####5. 使用make expr（和make asgn）之类的命令构造、编译L-expr（和L-asgn）分析器，并用所生成的分析器分析L-expr（和L-asgn）程序。
L-expr对一个输入的expression，输出它的值<br>
L-asgn相当于给L-expr添加了暂存器，可以用变量来暂存表达式的值(根本区别在于L-asgn维护了一个符号表，而L-expr没有维护符号表)<br>
（这道题好像没什么要答的，都是动手做的）

#test文件夹里几个c程序的说明
fibonacci.c求斐波那契数<br>
sort.c排序<br>
leftshift.c实现数组里元素的循环左移<br>
mdstack.c辗转相除法求最大公约数、最小公倍数，通过栈来实现参数的传递<br>
recurtion.c用递归的方式求阶乘<br>
errortest.c包含三种能处理的错误的各种形式<br>

#参考
看了ANSI-C的lex词法规范和yacc的词法规范，bison manual

#实现方式的说明
为了支持标准输入，lexer.l代码里有一段是对输入内容的记录，在记录过程中，如果碰到\t，就把它转换成四个空格来记录，这样可以在指示出错地点时不因\t而指错地方。每处理完一行，就把当前能判断出来的错误指示出来<br>
表达式缺运算符的情况通过exp ::= exp error exp来进行错误恢复。<br>
用exp ::= LB exp和exp ::= exp error RB 来处理缺少半边括号的错误<br>

#完成情况
都完成了

#处理的冲突
minus exp 和 exp minus exp的冲突通过指定前者优先级为UNARY，后者优先级为MINUS，而UNARY的优先级比MINUS高的方式来处理<br><br>
if condition statement 和 if condition statement else statement的冲突通过指定前者优先级为IF，后者优先级为ELSE，而ELSE的优先级比IF高的方式来处理<br><br>
expression的二义性通过指定算符的优先级和结合性来避免<br><br>
表达式缺运算符的情况通过exp ::= exp error exp来进行错误恢复。因这个规则产生的冲突通过指定error的优先级低于NUMBER和IDENTIFIER的方式来处理，优先移进。用exp ::= LB exp %prec ASGN和exp ::= exp error RB %prec ASGN来处理缺少半边括号的错误，这两条规则的优先级都指定为ASGN，这样可以避免冲突。<br><br>
最后所有冲突都被消除了。