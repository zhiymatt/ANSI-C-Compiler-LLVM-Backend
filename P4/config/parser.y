%{

#include <stdio.h>
//#define DEBUG
#include <util.h>

extern int yylex ();
extern void yyerror (const char *msg);
extern char currentline[200];

typedef struct YYLTYPE {
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1

int placenum = 0;
int placetype[64];
YYLTYPE place[64];
char inbuf[1024][256];
void specifyerror();

%}


%locations

%precedence error
%precedence NUMBER IDENTIFIER
%right ASGN
%left EQ NE
%left '<' '>' LE GE
%left DISMATCH
%left MINUS PLUS
%left MULT DIV MOD
%left UNARY
%left LB RB

%precedence IF
%precedence ELSE

%token NUMBER IDENTIFIER
%token INT VOID
%token CONST
%token IF ELSE
%token WHILE
%token ODD

%start sss

%%
sss			: funcdeflist
			  {
				debug("sss ::= funcdeflist\n");
			  	printf("\nsucceful!\n");
			  }
			| decllist funcdeflist
			  {
				debug("sss ::= decllist funcdeflist\n");
				printf("\nsucceful!\n");
			  }

decllist	: decl
			  {
				debug("decllist ::= decl\n");
			  }
			| decllist decl
			  {
				debug("decllist ::= decl decllist\n");
			  }
			;

decl		: constdecl
			  {
				debug("decl ::= constdecl\n");
			  }
			| vardecl
			  {
				debug("decl ::= vardecl\n");
			  }
			;

constdecl	: CONST INT constdeflist ';'
			  {
				debug("constdecl ::= CONST INT constdeflist ';'\n");
			  }
			| CONST constdeflist ';'
			  {
			    debug("constdecl ::= CONST error constdeflist ';'\n");
				place[placenum] = @2;
				placetype[placenum++] = 1;
			  }
			;

constdeflist: constdef
			  {
				debug("constdeflist ::= constdef\n");
			  }
			| constdeflist ',' constdef
			  {
				debug("constdeflist ::= constdeflist ',' constdef\n");
			  }
			;

constdef 	: IDENTIFIER ASGN exp
			  {
				debug("constdef ::= IDENTIFIER ASGN exp\n");
			  }
			| IDENTIFIER '[' exp ']' ASGN '{' explist '}' 
			  {
				debug("constdef ::= IDENTIFIER '[' exp ']' ASGN '{' explist '}'\n");
			  }
			| IDENTIFIER '[' ']' ASGN '{' explist '}' 
			  {
				debug("constdef ::= IDENTIFIER '[' ']' ASGN '{' explist '}'\n");
			  }
			;

vardecl		: INT varlist ';'
			  {
				debug("vardecl ::= INT varlist ';'\n");
			  }
			;

varlist		: var 
			  {
				debug("varlist ::= var\n");
			  }

			| varlist ',' var
			  {
				debug("varlist ::= varlist ',' var\n");
			  }
			;

var			: IDENTIFIER
			  {
				debug("var ::= IDENTIFIER\n");
			  }
			| IDENTIFIER '[' exp ']'
			  {
				debug("var ::= IDENTIFIER '[' exp ']'\n");
			  }
			| IDENTIFIER ASGN exp
			  {
				debug("var ::= IDENTIFIER ASGN exp\n");
			  }
			| IDENTIFIER '[' exp ']' ASGN '{' explist '}'
			  {
				debug("var ::= IDENTIFIER '[' exp ']' ASGN '{' explist '}'\n");
			  }
			; 

explist		: exp
			  {
				debug("explist ::= exp\n");
			  }
			| explist ',' exp
			  {
				debug("explist ::= explist ',' exp\n");
			  }
			;

funcdeflist	: funcdef
			  {
				debug("funcdeflist ::= funcdef\n");
			  }
			| funcdeflist funcdef
			  {
				debug("funcdeflist ::= funcdeflist funcdef\n");
			  }
			;

funcdef		: VOID IDENTIFIER LB RB block
			  {
				debug("funcdef ::= VOID IDENTIFIER LB RB block\n");
			  }
			| VOID IDENTIFIER RB block
			  {
			  	debug("funcdef ::= VOID IDENTIFIER error RB block\n");
				place[placenum] = @3;
				placetype[placenum++] = 2;
			  }
			| VOID IDENTIFIER LB block
			  {
			  	debug("funcdef ::= VOID IDENTIFIER LB error block\n");
				place[placenum] = @3;
				placetype[placenum++] = 2;
			  }
			;

block 		: '{' blockil '}'
			  {
				debug("block ::= '{' blockil '}'\n");
			  }
			| '{' '}'
			;

blockil		: blockitem
			  {
				debug("blockil ::= blockitem\n");
			  }
			| blockil blockitem
			  {
				debug("blockil ::= blockil blockitem\n");
			  }
			;

blockitem	: decl
			  {
				debug("blockitem ::= decl\n");
			  }
			| stmt
			  {
				debug("blockitem ::= stmt\n");
			  }			
			;

stmt 		: lval ASGN exp ';'
			  {
				debug("stmt ::= lval ASGN exp ';'\n");
			  }
			| IDENTIFIER LB RB ';'
			  {
				debug("stmt ::= IDENTIFIER LB RB ';'\n");
			  }
			| IDENTIFIER RB ';'
			  {
			  	debug("stmt ::= IDENTIFIER error RB ';'\n");
				place[placenum] = @2;
				placetype[placenum++] = 2;
			  }
			| IDENTIFIER LB ';'
			  {
			  	debug("stmt ::= IDENTIFIER LB error ';'\n");
				place[placenum] = @2;
				placetype[placenum++] = 2;
			  }
			| block
			  {
				debug("stmt ::= block\n");
			  }
			| IF LB cond RB stmt ELSE stmt %prec ELSE
			  {
				debug("stmt ::= IF LB cond RB stmt ELSE stmt\n");
			  }
			| IF LB cond stmt ELSE stmt %prec ELSE
			  {
			  	debug("stmt ::= IF LB cond error stmt ELSE stmt\n");
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| IF cond RB stmt ELSE stmt %prec ELSE
			  {
			  	debug("stmt ::= IF error cond RB stmt ELSE stmt\n");
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| IF LB cond RB stmt %prec IF
			  {
				debug("stmt ::= IF LB cond RB stmt \n");
			  }
			| IF LB cond stmt %prec IF
			  {
			  	debug("stmt ::= IF LB cond error stmt\n");
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| IF cond RB stmt %prec IF
			  {
			  	debug("stmt ::= IF error cond RB stmt\n");
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| WHILE LB cond RB stmt
			  {
				debug("stmt ::= WHILE LB cond RB stmt\n");
			  }
			| WHILE LB cond stmt
			  {
			  	debug("stmt ::= WHILE LB cond error stmt\n");
				place[placenum] = @4;
				placetype[placenum++] = 4;
			  }
			| WHILE cond RB stmt
			  {
			  	debug("stmt ::= WHILE error cond RB stmt\n");
				place[placenum] = @2;
				placetype[placenum++] = 4;
			  }
			| ';'
			  {
				debug("stmt ::= ';'\n");
			  }
			;

lval		: IDENTIFIER
			  {
				debug("lval ::= IDENTIFIER\n");
			  }
			| IDENTIFIER '[' exp ']'
			  {
				debug("lval ::= IDENTIFIER '[' exp ']'\n");
			  }
			;



cond
	: exp '<' exp %prec '<'
	{
		debug("cond ::= exp '<' exp\n");
	}
	| exp '>' exp %prec '>'
	{
		debug("cond ::= exp '>' exp\n");
	}
	| exp LE exp %prec LE
	{
		debug("cond ::= exp LE exp\n");
	}
	| exp GE exp %prec GE
	{
		debug("cond ::= exp GE exp\n");
	}
	| ODD exp %prec ASGN
	{
		debug("cond ::= ODD exp\n");
	}
	| exp EQ exp %prec EQ
	{
		debug("cond ::= exp EQ exp\n");
	}
	| exp NE exp %prec NE
	{
		debug("cond ::= exp NE exp\n");
	}
	;


exp
	: exp PLUS exp %prec PLUS
	  {
	  	debug("exp ::= exp PLUS exp\n");
	  }
	| exp MINUS exp %prec MINUS
	  {
	  	debug("exp ::= exp MINUS exp\n");
	  }
	| exp MULT exp %prec MULT
	  {
	  	debug("exp ::= exp MULT exp\n");
	  }
	| exp DIV exp %prec DIV
	  {
	  	debug("exp ::= exp DIV exp\n");
	  }
	| exp MOD exp %prec MOD
	  {
	  	debug("exp ::= exp MOD exp\n");
	  }
	| LB exp RB %prec LB
	  {
	  	debug("exp ::= LB exp RB\n");
	  }
	| MINUS exp %prec UNARY
	  {
	  	debug("exp ::= MINUS exp\n");
	  }
	| PLUS exp %prec UNARY
	  {
	  	debug("exp ::= PLUS exp\n");
	  }
	| NUMBER
	  {
	  	debug("exp ::= NUMBER\n");
	  }
	| lval
	  {
	  	debug("exp ::= lval\n");
	  }
	| LB exp %prec ASGN
	  {
	  	debug("exp ::= LB exp\n");
		place[placenum] = @1;
		placetype[placenum++] = 2;
	  }
	| exp error RB %prec ASGN
	  {
	  	debug("exp ::= exp error RB\n");
		place[placenum] = @2;
		placetype[placenum++] = 2;
	  }
	| exp error exp %prec MINUS
	  {
	    debug("exp ::= exp error exp\n");
	    place[placenum] = @2;
	    placetype[placenum++] = 3;
	  }
	;



%%

void yyerror (const char *msg)
{
    printf("%s\n", msg);
}

void specifyerror()
{
	int i;
	for (i = 0; i < placenum; i++)
	{
		switch (placetype[i])
		{
			case 1: 
				printf("<warning> expecting type here ");
				break;
			case 2: 
				printf("<error> mismatched LB or RB ");
				break;
			case 3: 
				printf("<error> expecting op here ");
				break;
			case 4: 
				printf("<error> expecting LB or RB here ");
				break;
			default:;
		}
		printf("in %d.%d-%d.%d\n%s\n", place[i].first_line, place[i].first_column,
			place[i].last_line, place[i].last_column, inbuf[place[i].first_line - 1]);
		for (int j = 1 ; j < place[i].first_column ; j++)
		{
			putchar('.');
		}
		printf("^\n");
	}
	placenum = 0;
}
