%{

    #include <stdio.h>
    #include <string.h>
    #include "parser.tab.h"
    static void comment(void);

    extern char inbuf[1024][256];
    extern void specifyerror();
    static void update_loc(){
        static int curr_line = 1;
        static int curr_col  = 1;
        yylloc.first_line   = curr_line;
        yylloc.first_column = curr_col;
        if (*yytext == '\t')
            strcat(inbuf[curr_line - 1], "    ");
        else if(*yytext == '\n')
            ;
        else
            strcat(inbuf[curr_line - 1], yytext);
        {
            char * s; 
            for(s = yytext; *s != '\0'; s++)
            {
                if(*s == '\n')
                {
                    specifyerror();
                    curr_line++;
                    curr_col = 1;
                }
                else if(*s == '\t')
                    curr_col = curr_col + 4;
                else
                    curr_col++;
            }
        }
        yylloc.last_line   = curr_line;
        yylloc.last_column = curr_col-1;
    }
    
    #define YY_USER_ACTION update_loc();

%}

O                   [0-7]
D                   [0-9]
NZ                  [1-9]
L                   [a-zA-Z_]
A                   [a-zA-Z_0-9]
H                   [a-fA-F0-9]
HP                  (0[xX])
E                   ([Ee][+-]?{D}+)
P                   ([Pp][+-]?{D}+)
FS                  (f|F|l|L)
IS                  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP                  (u|U|L)
SP                  (u8|u|U|L)
ES                  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS                  [ \t\n\v\f]

%%

"/*"	                { 
                        comment(); 
                        }
"//".*	                { 
                        } 

"const"                 {
                            return CONST; 
                        }
"else"		            { 
                            return ELSE; 
                        }
"if"                    { 
                            return IF; 
                        }
"int"                   { 
                            return INT; 
                        }
"void"		            { 
                            return VOID; 
                        }
"while"                 { 
                            return WHILE; 
                        }
"odd"                   { 
                            return ODD; 
                        }

{L}{A}*		                    { 
                                    return IDENTIFIER; 
                                }
{HP}{H}+{IS}?					{ 
                                    return NUMBER; 
                                }
{NZ}{D}*{IS}?					{ 
                                    return NUMBER; 
                                }
"0"{O}*{IS}?					{ 
                                    return NUMBER; 
                                }
{CP}?"'"([^'\\\n]|{ES})+"'"	    { 
                                    return NUMBER; 
                                }

"<="	{ return LE; }
">="	{ return GE; }
"=="	{ return EQ; }
"!="	{ return NE; }
">"     { return '>'; }
"<"     { return '<'; }
";"		{ return ';'; }
"{"		{ return '{'; }
"}"		{ return '}'; }
","		{ return ','; }
"="		{ return ASGN; }
"("		{ return LB; }
")"		{ return RB; }
"["		{ return '['; }
"]"		{ return ']'; }
"+"		{ return PLUS; }
"-"		{ return MINUS; }
"*"		{ return MULT; }
"/"		{ return DIV; }
"%"		{ return MOD; }

{WS}	{ }





%%

int yywrap(void)
{
	return 1;
}

static void comment(void)
{
    int c;
	while ((c = input()) != 0)
        if (c == '*')
        {
            while ((c = input()) == '*')
                ;
			if (c == '/')
                return;
			if (c == 0)
                break;
        }
}